module SquareJ1J2Jc

export make_square_lattice
export pauli_matrix
export make_J1J2Jc_hamiltonian
export make_Sz_Sz_correlators

using LinearAlgebra
using ExactDiagonalization
using TightBindingLattice



function pauli_matrix(hs::HilbertSpace, isite ::Integer, j ::Symbol)
  if j == :x
    return pure_operator(hs, isite, 1, 2, 1.0; dtype=UInt) + pure_operator(hs, isite, 2, 1, 1.0; dtype=UInt)
  elseif j == :y
    return pure_operator(hs, isite, 1, 2, -1.0im; dtype=UInt) + pure_operator(hs, isite, 2, 1, 1.0im; dtype=UInt)
  elseif j == :z
    return pure_operator(hs, isite, 1, 1, 1.0; dtype=UInt) + pure_operator(hs, isite, 2, 2, -1.0; dtype=UInt)
  elseif j == :+
    return pure_operator(hs, isite, 1, 2, 1.0; dtype=UInt)
  elseif j == :-
    return pure_operator(hs, isite, 2, 1, 1.0; dtype=UInt)
  else
    throw(ArgumentError("pauli matrix of type $(j) not supported"))
  end
end

#
# function make_square_lattice(n1 ::Integer, n2 ::Integer)
#
#   s2i(sub) = mod(sub[1]-1, n1) * n2 + mod(sub[2]-1, n2) + 1
#   i2s(i) = [mod((i-1) ÷ n2, n1) + 1, mod(i-1, n2) + 1]
#
#   n_sites = n1 * n2
#   coordinates = [i2s(i) for i in 1:n_sites]
#
#   nearest_neighbor_bond_types = [(1,0), (0,1)]
#   nearest_neighbor_pairs = Tuple{Int, Int, Tuple{Int, Int}}[]
#   for i1 in 1:n1, i2 in 1:n2
#     i = s2i([i1, i2])
#     for (d1, d2) in nearest_neighbor_bond_types
#       j = s2i([i1+d1, i2+d2])
#       push!(nearest_neighbor_pairs, (i,j, (d1,d2)))
#     end
#   end
#
#   second_nearest_neighbor_bond_types = [(1,1), (-1,1)]
#   second_nearest_neighbor_pairs = Tuple{Int, Int, Tuple{Int, Int}}[]
#   for i1 in 1:n1, i2 in 1:n2
#     i = s2i([i1, i2])
#     for (d1, d2) in second_nearest_neighbor_bond_types
#       j = s2i([i1+d1, i2+d2])
#       push!(second_nearest_neighbor_pairs, (i,j, (d1,d2)))
#     end
#   end
#
#   #=
#         o
#       / | \
#     o - o - o
#       \ | /
#         o
#   =#
#   chiral_triplet_types = [
#       (( 1, 0), ( 0, 1)),
#       (( 0, 1), (-1, 0)),
#       ((-1, 0), ( 0,-1)),
#       (( 0,-1), ( 1, 0)),
#   ]
#   chiral_triplets = Tuple{Int, Int, Int, Tuple{Int, Int}, Tuple{Int, Int}}[]
#
#   for i1 in 1:n1, i2 in 1:n2
#     i = s2i([i1, i2])
#     for ((d1, d2), (e1, e2)) in chiral_triplet_types
#       j = s2i([i1+d1, i2+d2])
#       k = s2i([i1+e1, i2+e2])
#       push!(chiral_triplets, (i,j,k, (d1,d2), (e1,e2)))
#     end
#   end
#
#   t1 = Permutation([ s2i( i2s(i)+[1,0] ) for i in 1:n_sites])
#   t2 = Permutation([ s2i( i2s(i)+[0,1] ) for i in 1:n_sites])
#   trans_group = TranslationGroup([t1, t2])
#
#   return (coordinates, nearest_neighbor_pairs, second_nearest_neighbor_pairs, chiral_triplets, trans_group)
# end
#
#

#=
      o
    / | \
  o - o - o
    \ | /
      o
=#
function make_bonds(hypercube ::HypercubicLattice)
  n_sites = length(hypercube.coordinates)
  i2s(i::Integer) = hypercube.coordinates[i]
  s2i(sub::AbstractVector{<:Integer}) = hypercube.torus_wrap(sub)[2]

  nearest_neighbor_bond_types = [(1,0), (0,1)]
  nearest_neighbor_pairs = Tuple{Int, Int, Tuple{Int, Int}}[]
  for i in 1:n_sites, (d1, d2) in nearest_neighbor_bond_types
    i1, i2 = i2s(i)
    j = s2i([i1+d1, i2+d2])
    push!(nearest_neighbor_pairs, (i,j, (d1,d2)))
  end

  second_nearest_neighbor_bond_types = [(1,1), (-1,1)]
  second_nearest_neighbor_pairs = Tuple{Int, Int, Tuple{Int, Int}}[]
  for i in 1:n_sites, (d1, d2) in second_nearest_neighbor_bond_types
    i1, i2 = i2s(i)
    j = s2i([i1+d1, i2+d2])
    push!(second_nearest_neighbor_pairs, (i,j, (d1,d2)))
  end

  chiral_triplet_types = [(( 1, 0), ( 0, 1)), (( 0, 1), (-1, 0)), ((-1, 0), ( 0,-1)), (( 0,-1), ( 1, 0)),]
  chiral_triplets = Tuple{Int64, Int64, Int64, Tuple{Int, Int}, Tuple{Int, Int}}[]
  for i in 1:n_sites, ((d1, d2), (e1, e2)) in chiral_triplet_types
    i1, i2 = i2s(i)
    j = s2i([i1+d1, i2+d2])
    k = s2i([i1+e1, i2+e2])
    push!(chiral_triplets, (i,j,k, (d1,d2), (e1,e2)))
  end

  return (nearest_neighbor_pairs, second_nearest_neighbor_pairs, chiral_triplets)
end



"""
J₁-J₂-Jᵪ Hamiltonian

```math
H = J_1 \\sum_{\\langle i, j \\rangle} S_{i} \\cdot S_{j} + ...
```
"""
function make_J1J2Jc_hamiltonian(hypercube ::HypercubicLattice; theta1::Real=0.0, theta2::Real=0.0)
  n_sites = length(hypercube.coordinates)
  hopping_phases = ones(ComplexF64, (3, 3))
  # TODO: check whether to transpose or not
  (theta_x, theta_y) = 2*pi*transpose(hypercube.inverse_scale_matrix) * [theta1, theta2]
  for d1 in -1:1, d2 in -1:1
    hopping_phases[2+d1, 2+d2] = cis(d1 * theta_x + d2 * theta_y)
  end

  hopping_phase(d1 ::Int, d2 ::Int) ::ComplexF64 = return hopping_phases[2+d1, 2+d2]

  spin_site = Site([State("Up", 1), State("Dn", -1)])
  hs = HilbertSpace([spin_site for i in 1:n_sites])
  σ(i ::Integer, j ::Symbol) = pauli_matrix(hs, i, j)

  (nearest_neighbor_pairs, second_nearest_neighbor_pairs, chiral_triplets) = make_bonds(hypercube)

  j1_terms = []
  for (i,j, (d1,d2)) in nearest_neighbor_pairs
    push!(j1_terms, σ(i, :+) * σ(j, :-) * (0.5 * hopping_phase(-d1, -d2)) )
    push!(j1_terms, σ(i, :-) * σ(j, :+) * (0.5 * hopping_phase(+d1, +d2)) )
    push!(j1_terms, σ(i, :z) * σ(j, :z) * 0.25)
  end

  j2_terms = []
  for (i,j, (d1,d2)) in second_nearest_neighbor_pairs
    push!(j2_terms, σ(i, :+) * σ(j, :-) * (0.5 * hopping_phase(-d1, -d2)) )
    push!(j2_terms, σ(i, :-) * σ(j, :+) * (0.5 * hopping_phase(+d1, +d2)) )
    push!(j2_terms, σ(i, :z) * σ(j, :z) * 0.25)
  end

  jc_terms  =[]
  for (i, j, k, (d1, d2), (e1, e2)) in chiral_triplets
    push!(jc_terms, σ(i, :z) * σ(j, :+) * σ(k, :-) * ( 0.25*im * hopping_phase(d1-e1, d2-e2)) )
    push!(jc_terms, σ(i, :z) * σ(j, :-) * σ(k, :+) * (-0.25*im * hopping_phase(e1-d1, e2-d2)) )

    push!(jc_terms, σ(j, :z) * σ(k, :+) * σ(i, :-) * ( 0.25*im * hopping_phase( e1,  e2)) )
    push!(jc_terms, σ(j, :z) * σ(k, :-) * σ(i, :+) * (-0.25*im * hopping_phase(-e1, -e2)) )

    push!(jc_terms, σ(k, :z) * σ(i, :+) * σ(j, :-) * ( 0.25*im * hopping_phase(-d1, -d2)) )
    push!(jc_terms, σ(k, :z) * σ(i, :-) * σ(j, :+) * (-0.25*im * hopping_phase( d1,  d2)) )
  end

  j1 = simplify(sum(j1_terms))
  j2 = simplify(sum(j2_terms))
  jc = simplify(sum(jc_terms))

  total_sx = sum(σ(i, :x) for i in 1:n_sites) * 0.5
  total_sy = sum(σ(i, :y) for i in 1:n_sites) * 0.5
  total_sz = sum(σ(i, :z) for i in 1:n_sites) * 0.5

  total_ssq = simplify(total_sx * total_sx + total_sy * total_sy + total_sz * total_sz)

  return (hs, j1, j2, jc, total_ssq)
end


"""
J₁-J₂-Jᵪ Hamiltonian

```math
H = J_1 \\sum_{\\langle i, j \\rangle} S_{i} \\cdot S_{j} + ...
```
"""
function make_Sz_Sz_correlators(hypercube ::HypercubicLattice, hs ::HilbertSpace{Int})
  n_sites = length(hypercube.coordinates)
  i2s(i::Integer) = hypercube.coordinates[i] ::Vector{Int}
  s2i(sub::AbstractVector{<:Integer}) = hypercube.torus_wrap(sub)[2] ::Int
  σ(i ::Integer, j ::Symbol) = pauli_matrix(hs, i, j)

  Sz_Sz_correlators = Dict{Tuple{Int, Int}, AbstractOperator}()
  for (d1, d2) in hypercube.coordinates
    operator_terms = []
    for (i, (i1, i2)) in enumerate(hypercube.coordinates)
      j = s2i([i1+d1,i2+d2])
      push!(operator_terms, σ(i, :z) * σ(j, :z))
    end
    Sz_Sz_correlators[d1, d2] = simplify(sum(operator_terms) * (0.25 / (n_sites)))
  end

  return Sz_Sz_correlators
end



end # module
