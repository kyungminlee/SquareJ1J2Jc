using LinearAlgebra
using SparseArrays
using Arpack
using JLD2
using ExactDiagonalization
using TightBindingLattice
using ArgParse
using Memento
import Formatting
using DataStructures
using JSON
using HDF5
using Glob

main_logger = Memento.config!("info"; fmt="[{date} | {level} | {name}]: {msg}")

include("src/SquareJ1J2Jc.jl")

import .SquareJ1J2Jc.make_J1J2Jc_hamiltonian
import .SquareJ1J2Jc.make_Sz_Sz_correlators


function parse_commandline()
  s = ArgParseSettings()
  @add_arg_table s begin
    "directories"
      arg_type = String
      nargs = '+'
  end
  return parse_args(s)
end

function pushd(f::Function, dir ::AbstractString)
  current_dir = pwd()
  real_dir = realpath(dir)
  try
    cd(real_dir)
    f()
  finally
    cd(current_dir)
  end
end

BN(x::Integer) = Formatting.format(x, commas=true)

function run(;size_matrix ::Matrix{Int},
             twoSz ::Int,
             J1 ::Float64, J2 ::Float64, Jc ::Float64,
             theta1 ::Float64, theta2 ::Float64,
             num_eigens ::Int,
             observable_names ::Vector{String},
             save_eigenvectors ::Bool,
             parameters)

  hypercube = HypercubicLattice(size_matrix)

  info(main_logger, "Parameteters : shape = $size_matrix, 2Sz = $(twoSz), θ₁ = $(theta1), θ₂ = $(theta2) (in units of 2π)")

  info(main_logger, "Making J₁, J₂, Jᵪ terms")
  (hs, j1, j2, jc, total_ssq) = make_J1J2Jc_hamiltonian(hypercube; theta1=theta1, theta2=theta2)

  info(main_logger, "Combining Hamiltonian")
  H = J1 * j1 + J2 * j2 + Jc * jc

  info(main_logger, "Finding Translation Symmetry Group of the given hypercube")
  trans_group = translation_symmetry_group(hypercube)
  info(main_logger, "Translation group has $(length(trans_group.generators)) generators")
  info(main_logger, "  Orders of generators: $([g.order for g in trans_group.generators])")
  info(main_logger, "There are $(length(trans_group.fractional_momenta)) momenta allowed")

  info(main_logger, "Checking whether the Hamiltonian is translationally invariant")
  @assert is_invariant(hs, trans_group, H)

  info(main_logger, "Representing Hilbert space"); flush(stdout)
  hsr = represent(HilbertSpaceSector(hs, twoSz))
  info(main_logger, "Hilbert space dimension = $(BN(length(hsr.basis_list)))")

  observables = NamedTuple{(:name, :operator, :isreal), Tuple{String, AbstractOperator, Bool}}[]

  if "spin_squared" in observable_names
    push!(observables, (name="spin_squared", operator=total_ssq, isreal=true))
  end

  if "sz_sz_correlators" in observable_names
    sz_sz_correlators = make_Sz_Sz_correlators(hypercube, hs)
    for (d1, d2) in hypercube.coordinates
      push!(observables, (name="sz_sz_correlator[$d1,$d2]", operator=sz_sz_correlators[d1,d2], isreal=true))
    end
  end

  info(main_logger, "Checking whether every observable is translationally invariant")
  for (name, operator, isreal) in observables
    info(main_logger, "  $name")
    @assert is_invariant(hs, trans_group, operator)
  end

  for kf in trans_group.fractional_momenta
    info(main_logger, "Symmetry-reducing Hilbert space for momentum $(kf)")

    rhsr = symmetry_reduce_parallel(hsr, trans_group, kf)
    info(main_logger, "Reduced Hilbert space dimension = $(BN(length(rhsr.basis_list)))")

    info(main_logger, "Sparse-representing Hamiltonian"); flush(stdout)
    H_rep = represent(rhsr, H)
    H_sparse = sparse(H_rep)
    info(main_logger, "Number of nonzero values = $(BN(length(H_sparse.nzval)))")

    observable_sparses = AbstractSparseArray[]
    sizehint!(observable_sparses, length(observables))
    for (name, operator, isreal) in observables
      info(main_logger, "Sparse-representing $name"); flush(stdout)
      operator_rep = represent(rhsr, operator)
      operator_sparse = sparse(operator_rep)

      push!(observable_sparses, operator_sparse)
      info(main_logger, "Number of nonzero values = $(BN(length(operator_sparse.nzval)))")
    end

    @assert all(size(op) == size(H_sparse) for op in observable_sparses)

    GC.gc()
    
    eigenvalues = nothing
    eigenvectors = nothing
    observable_expectations = []
    matrix_size = size(H_sparse)[1]
    if matrix_size <= max(400, num_eigens)
      info(main_logger, "Diagonalizing dense Hamiltonian (size = $(BN(matrix_size)))")
      (eigenvalues, eigenvectors) = eigen(Hermitian(Matrix(H_sparse)))
      idx = sortperm(eigenvalues)[1:min(matrix_size, num_eigens)]
      eigenvalues = eigenvalues[idx]
      eigenvectors = eigenvectors[:, idx]
    else
      info(main_logger, "Diagonalizing sparse Hamiltonian (size = $(BN(matrix_size)))")
      (eigenvalues, eigenvectors) = eigs(H_sparse; which=:SR, nev=num_eigens)
      eigenvalues = real.(eigenvalues)
      idx = sortperm(eigenvalues)
      eigenvalues = eigenvalues[idx]
      eigenvectors = eigenvectors[:, idx]
    end
    neig = length(eigenvalues)

    if !isempty(observables)
      info(main_logger, "Computing $(length(observables)) observables")
      for (iobs, ((name, operator, isreal), operator_sparse)) in enumerate(zip(observables, observable_sparses))
        info(main_logger, "  Observable $iobs: $name")
        O = [(dot(eigenvectors[:,i], operator_sparse * eigenvectors[:,i])) for i in 1:neig]
        if isreal
          push!(observable_expectations, real.(O))
        else
          push!(observable_expectations, O)
        end
      end
    end

    slim(d ::AbstractDict) = Dict(slim(k) => slim(v) for (k, v) in d)
    slim(a ::AbstractArray) = [slim(i) for i in a]
    slim(x ::Number) = x
    slim(x ::AbstractString) = String(x)
    slim(x ::Bool) = Int(x)

    group_generator_orders = [g.order for g in trans_group.generators]

    parameters["observables"] = Vector{String}(parameters["observables"])
    parameters = slim(parameters)
    momentum_index = [Int(k*n) for (k, n) in zip(kf, group_generator_orders)]
    parameters["momentum_index"] = momentum_index

    groupname_format = Formatting.FormatExpr("{:d}_{:s}")
    groupname = Formatting.format(groupname_format, twoSz, join(momentum_index, "x"))
    filename = "spectrum_" * groupname * ".hdf5"

    info(main_logger, "Saving $filename")
    h5open(filename, "w") do file
      g = g_create(file, groupname)
      for (key, val) in parameters
        attrs(g)[key] = slim(val)
      end
      g["eigenvalues"] = eigenvalues
      g["eigenvectors", "chunk", (matrix_size, neig), "compress", 9] = eigenvectors
      g_obs = g_create(g, "observables")
      for ((name, operator, isreal), Oexp) in zip(observables, observable_expectations)
        g_obs[name] = Oexp
      end
    end # h5open

  end # for kf

end

function main()
  args = parse_commandline()

  info(main_logger, "Number of threads = $(Threads.nthreads())")
  directories ::Vector{String} = if Sys.iswindows()
    vcat([Glob.glob(d) for d in args["directories"]]...)
  else
    args["directories"]
  end

  for directory in directories

    info(main_logger, "Processing $directory")
    pushd(directory) do
      parameters = JSON.parsefile("parameters.json")

      shape = parameters["shape"]
      size_matrix ::Matrix{Int} = hcat(shape...)
      @assert size(size_matrix) == (2,2)
      parameters["shape"] = transpose(size_matrix)

      twoSz ::Int = parameters["twoSz"]
      J1 ::Float64, J2 ::Float64, Jc ::Float64 = parameters["J1"], parameters["J2"], parameters["Jc"]
      theta1 ::Float64, theta2 ::Float64 = parameters["theta"]
      num_eigens ::Int = parameters["num_eigens"]
      observable_names ::Vector{String} = parameters["observables"]
      save_eigenvectors ::Bool = parameters["save_eigenvectors"]

      for twoSz in [0, 2, 4, 6, 8]
        run(size_matrix=size_matrix,
            twoSz=twoSz,
            J1=J1, J2=J2, Jc=Jc,
            theta1=theta1, theta2=theta2,
            num_eigens=num_eigens,
            observable_names=observable_names,
            save_eigenvectors=save_eigenvectors,
            parameters=parameters)

      end
    end # pushd

  end # for directory

end # main

main()
