using LinearAlgebra
using SparseArrays
using Arpack
using ExactDiagonalization
using ArgParse
import Formatting
using DataStructures


include("src/SquareJ1J2Jc.jl")

hsr = nothing
rhsr = nothing
H = nothing

function main()
  n1, n2 = 4, 4
  qn = 0
  J1 = 1.0
  J2 = 0.5
  Jc = 0.25
  theta1 = 0.0
  theta2 = 0.0
  num_eigens = 64

  (hs, j1, j2, jc, total_ssq, trans_group) = make_J1J2Jc_hamiltonian(n1, n2; theta1=theta1*(2π), theta2=theta2*(2π))

  @assert is_invariant(trans_group, j1)
  @assert is_invariant(trans_group, j2)
  @assert is_invariant(trans_group, jc)

  hsr = represent(HilbertSpaceSector(hs, Set([qn])))

  all_eigenvalues = Float64[]
  all_spin_squared = Float64[]

  for kf in trans_group.fractional_momenta
    rhsr = symmetry_reduce_parallel(hsr, trans_group, kf)

    j1_sparse, ϵ = materialize_parallel(rhsr, j1)
    j2_sparse, ϵ = materialize_parallel(rhsr, j2)
    jc_sparse, ϵ = materialize_parallel(rhsr, jc)
    spin_sq_sparse, ϵ = materialize_parallel(rhsr, total_ssq)
    global H
    H = J1 * j1_sparse + J2 * j2_sparse + Jc * jc_sparse
    # continue
    break

    eigenvalues = nothing
    eigenvectors = nothing
    spin_squared = nothing

    if size(H)[1] <= 200
      (eigenvalues, eigenvectors) = eigen(Hermitian(Matrix(H)))
    else
      (eigenvalues, eigenvectors) = eigs(H; which=:SR, nev=num_eigens)
      eigenvalues = real.(eigenvalues)
    end
    idx = sortperm(eigenvalues)
    eigenvalues = eigenvalues[idx]
    eigenvectors = eigenvectors[:, idx]
    neig = length(eigenvalues)
    append!(all_eigenvalues, eigenvalues)

    spin_squared = [real(dot(eigenvectors[:,i], spin_sq_sparse * eigenvectors[:,i])) for i in 1:neig]

    append!(all_spin_squared, spin_squared)
  end
end
