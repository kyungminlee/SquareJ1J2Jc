using ExactDiagonalization
using TightBindingLattice

include("src/SquareJ1J2Jc.jl")

import .SquareJ1J2Jc.make_J1J2Jc_hamiltonian
import .SquareJ1J2Jc.make_Sz_Sz_correlators
import .SquareJ1J2Jc.make_square_lattice

function main()
  size_matrix = [3 -1;
                 1 2]
  (coordinates, nearest_neighbor_pairs, second_nearest_neighbor_pairs, chiral_triplets, translation_group) = make_square_lattice(size_matrix)
  @show coordinates
  println("Coordinates")
  for (i, r) in enumerate(coordinates)
    println("$i: $r")
  end

  println("Nearest Neighbors")
  for (i, j, dis) in nearest_neighbor_pairs
    println("$(coordinates[i]) -> $(coordinates[j]) | $dis")
  end

  println("Second Nearest Neighbors")
  for (i, j, dis) in second_nearest_neighbor_pairs
    println("$(coordinates[i]) -> $(coordinates[j]) | $dis")
  end

  println("Chiral Triplets")
  for (i, j, k, dis1, dis2) in chiral_triplets
    println("$(coordinates[i]) -> $(coordinates[j]) -> $(coordinates[k]) | $dis1 × $dis2 ")
  end
end

main()
