using LinearAlgebra
using SparseArrays
using Arpack
using JLD2
using ExactDiagonalization
using ArgParse
using Memento
import Formatting
using DataStructures
using JSON3

main_logger = Memento.config!("info"; fmt="[{date} | {level} | {name}]: {msg}")

include("src/SquareJ1J2Jc.jl")

import SquareJ1J2Jc.make_J1J2Jc_hamiltonian

function parse_commandline()
  s = ArgParseSettings()
  @add_arg_table s begin
    "--n1"
      arg_type = Int
      required = true
    "--n2"
      arg_type = Int
      required = true
    "--twosz"
      arg_type = Int
      required = true
    "--J1"
      arg_type = Float64
      nargs = '+'
      default = [1.0]
    "--J2"
      arg_type = Float64
      nargs = '+'
      required = true
    "--Jc"
      arg_type = Float64
      nargs = '+'
      required = true
    "--theta1"
      arg_type = Float64
      default = 0.0
    "--theta2"
      arg_type = Float64
      default = 0.0
    "--observables"
      arg_type = String
      nargs = '*'
    "--save_eigenvectors"
      action = :store_true
    "--num_eigens"
      arg_type = Int
      default = 64
  end
  return parse_args(s)
end

function main()
  filename_format = Formatting.FormatExpr("spectrum_{:d}_{:d}_{:d}_{:.3f}_{:.3f}_{:.3f}_{:.3f}_{:.3f}")

  args = parse_commandline()

  n1 = args["n1"]
  n2 = args["n2"]
  qn = args["twosz"]
  J1s = args["J1"]
  J2s = args["J2"]
  Jcs = args["Jc"]
  theta1 = args["theta1"]
  theta2 = args["theta2"]
  num_eigens = args["num_eigens"]

  info(main_logger, "Number of threads = $(Threads.nthreads())")
  info(main_logger, "Parameteters : n1 = $n1, n2 = $n2, 2Sz = $(qn), θ₁ = $(theta1), θ₂ = $(theta2) (in units of 2π)")
  info(main_logger, "Making J₁, J₂, Jᵪ terms")

  (hs, j1, j2, jc, total_ssq, trans_group) = make_J1J2Jc_hamiltonian(n1, n2; theta1=theta1*(2π), theta2=theta2*(2π))

  info(main_logger, "Checking whether J₁, J₂, Jᵪ terms are translationally invariant")
  @assert is_invariant(trans_group, j1)
  @assert is_invariant(trans_group, j2)
  @assert is_invariant(trans_group, jc)

  BN(x::Integer) = Formatting.format(x, commas=true)

  info(main_logger, "Representing Hilbert space"); flush(stdout)
  hsr = represent(HilbertSpaceSector(hs, qn))
  info(main_logger, "Hilbert space dimension = $(BN(length(hsr.basis_list)))")

  info(main_logger, "There are $(length(trans_group.fractional_momenta)) momenta allowed")

  all_eigenvalues = DefaultDict(Vector{Float64})
  all_spin_squared = DefaultDict(Vector{Float64})

  for kf in trans_group.fractional_momenta
    info(main_logger, "Symmetry-reducing Hilbert space for momentum $(kf)")

    rhsr = symmetry_reduce_parallel(hsr, trans_group, kf)
    #rhsr = symmetry_reduce(hsr, trans_group, kf)
    info(main_logger, "Reduced Hilbert space dimension = $(BN(length(rhsr.basis_list)))")

    info(main_logger, "Materializing J₁"); flush(stdout)
    j1_sparse, ϵ = materialize_parallel(rhsr, j1)
    info(main_logger, "Number of nonzero values = $(BN(length(j1_sparse.nzval)))")
    info(main_logger, "Bound for error = $ϵ")

    info(main_logger, "Materializing J₂"); flush(stdout)
    j2_sparse, ϵ = materialize_parallel(rhsr, j2)
    info(main_logger, "Number of nonzero values = $(BN(length(j2_sparse.nzval)))")
    info(main_logger, "Bound for error = $ϵ")

    info(main_logger, "Materializing Jᵪ"); flush(stdout)
    jc_sparse, ϵ = materialize_parallel(rhsr, jc)
    info(main_logger, "Number of nonzero values = $(BN(length(jc_sparse.nzval)))")
    info(main_logger, "Bound for error = $ϵ")

    if "SpinSquared" in args["observables"]
      info(main_logger, "Materializing S⃗ ² ≡ S(S+1)"); flush(stdout)
      spin_sq_sparse, ϵ = materialize_parallel(rhsr, total_ssq)
      info(main_logger, "Number of nonzero values = $(BN(length(spin_sq_sparse.nzval)))")
      info(main_logger, "Bound for error = $ϵ")
    end

    for J1 in J1s, J2 in J2s, Jc in Jcs
      filename = Formatting.format(filename_format, n1, n2, qn, J1, J2, Jc, theta1, theta2) * ".json"
      if isfile(filename)
        info(main_logger, "File $filename exists. Skipping.")
        continue
      end

      info(main_logger, "Starting (J₁, J₂, Jᵪ) = ($J1, $J2, $Jc)")
      info(main_logger, "Constructing the total Hamiltonian")
      H = J1 * j1_sparse + J2 * j2_sparse + Jc * jc_sparse

      eigenvalues = nothing
      eigenvectors = nothing
      spin_squared = nothing
      if size(H)[1] <= 200
        info(main_logger, "Diagonalizing dense Hamiltonian (size = $(BN(size(H)[1])))")
        (eigenvalues, eigenvectors) = eigen(Hermitian(Matrix(H)))
      else
        info(main_logger, "Diagonalizing sparse Hamiltonian (size = $(BN(size(H)[1])))")
        (eigenvalues, eigenvectors) = eigs(H; which=:SR, nev=num_eigens)
        eigenvalues = real.(eigenvalues)
      end
      idx = sortperm(eigenvalues)
      eigenvalues = eigenvalues[idx]
      eigenvectors = eigenvectors[:, idx]
      neig = length(eigenvalues)
      append!(all_eigenvalues[J1, J2, Jc], eigenvalues)

      if "SpinSquared" in args["observables"]
        spin_squared = [real(dot(eigenvectors[:,i], spin_sq_sparse * eigenvectors[:,i])) for i in 1:neig]

        append!(all_spin_squared[J1, J2, Jc], spin_squared)
      end
    end
  end

  for (J1, J2, Jc) in keys(all_eigenvalues)
  #for J1 in J1s, J2 in J2s, Jc in Jcs
    filename = Formatting.format(filename_format, n1, n2, qn, J1, J2, Jc, theta1, theta2) * ".json"
    if isfile(filename)
      info(main_logger, "File $filename exists. Skipping.")
      continue
    end
    eigenvalues = all_eigenvalues[J1, J2, Jc]

    parameters = OrderedDict(
      "Shape" => [n1, n2],
      "TwoSz" => qn,
      "J1" => J1,
      "J2" => J2,
      "Jc" => Jc,
      "theta1" => theta1,
      "theta2" => theta2
    )


    results = OrderedDict(
        "Eigenvalues" => eigenvalues,
      )

    if "SpinSquared" in args["observables"]
      spin_squared = all_spin_squared[J1, J2, Jc]
      results["SpinSquared"] = spin_squared
    end

    open(filename, "w") do fp
      JSON3.write(fp, OrderedDict(
        "Parameters" => parameters,
        "Results" => results
      ))
    end
    #@save filename n1 n2 qn J1 J2 Jc  theta1 theta2 eigenvalues total_spin
  end # for J2, Jc
end

main()
