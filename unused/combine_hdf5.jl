using HDF5
using ArgParse

function parse_commandline()
  s = ArgParseSettings()
  @add_arg_table s begin
    "input"
      arg_type = String
      nargs = '+'
      required = true
    "--out", "-o"
      arg_type = String
      required = true
  end
  return parse_args(s)
end

"""
target[name] = source
"""
function h5copy!(target ::HDF5.DataFile, name ::AbstractString, source ::HDF5.DataFile)
  g_create(target, name)
  for attrname in names(attrs(source))
    attrs(target[name])[attrname] = read(attrs(source), attrname)
  end
  for childname in names(source)
    h5copy!(target[name], childname, source[childname])
  end
  flush(target)
end

function h5copy!(target ::HDF5.DataFile, name ::AbstractString, source ::HDF5Dataset)
  for attrname in names(attrs(source))
    attrs(target[name])[attrname] = read(attrs(source), attrname)
  end
  target[name] = read(source)
  flush(target)
end

function main()
  args = parse_commandline()

  outputfilename = args["out"]
  if isfile(args["out"])
    print("File $(outputfilename) exists. Overwrite? (y/N) ")
    yn = readline()
    if isempty(yn) || (yn[1] != 'y' && yn[1] != 'Y')
      println("Quitting")
      exit(1)
    end
  end

  h5open(outputfilename, "w") do outputf
    global_attributes = Dict{String, Any}()
    for inputfilename in args["input"]
      print("Copying $inputfilename to $outputfilename")
      h5open(inputfilename, "r") do inputf
        for name in names(attrs(inputf))
          attrs(outputf)[name] = read(attrs(inputf), name)
        end
        for name in names(inputf)
          h5copy!(outputf, name, inputf[name] )
        end
      end # h5open(inputfilename)
      println(".")
    end # for inputfilename
  end #h5open(outputfilename)

end

main()
