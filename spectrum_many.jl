# initial_memory = Base.gc_num()
#initial_free_memory = Sys.free_memory()

using LinearAlgebra
using SparseArrays
using InteractiveUtils
using Logging
using Dates

using Arpack
using JLD2
using FileIO
using ArgParse
#using Memento
import Formatting
using DataStructures
using JSON
using HDF5
using Glob

using ExactDiagonalization
using TightBindingLattice

function get_rss()
  result = Vector{Csize_t}(undef, 1)
  ccall(:uv_resident_set_memory, Cint, (Ptr{Csize_t},), Ref(result,1))
  return result[1]
end


function my_metafmt(level, _module, group, id, file, line)
  color = Logging.default_logcolor(level)
  #prefix = (level == Logging.Warn ? "Warning" : string(level))*':'
  #prefix = string(Dates.format(Dates.now(), "yyyy-mm-ddTHH:MM:SS" ))* " | " * string(level) * " | " * string(id) * " | RSS: $(BN(get_rss())) | "
  date_string = Dates.format(Dates.now(), "yyyy-mm-ddTHH:MM:SS.sss")
  prefix = "$(date_string) | $(level) | $(id) | RSS: $(BN(get_rss())) | "
  suffix = ""
  Logging.Info <= level < Logging.Warn && return color, prefix, suffix
  _module !== nothing && (suffix *= "$(_module)")
  if file !== nothing
      _module !== nothing && (suffix *= " ")
      suffix *= Base.contractuser(file)
      if line !== nothing
          suffix *= ":$(isa(line, UnitRange) ? "$(first(line))-$(last(line))" : line)"
      end
  end
  !isempty(suffix) && (suffix = "@ " * suffix)
  return color, prefix, suffix
end


#global main_logger = Memento.config!("info"; fmt="[{date} | {level} | {name}]: {msg}")
#global ed_logger = Memento.getlogger("ExactDiagonalization")
#log(msg::AbstractString) = info(main_logger, "[RSS: $(get_rss())] "*msg)
function log(msg::AbstractString) 
  @info msg
  flush(stdout)
end

include("src/SquareJ1J2Jc.jl")


using .SquareJ1J2Jc: make_lattice,
                     make_bonds,
                     make_hilbert_space,
                     make_hamiltonian_terms,
                     make_spin_squared,
                     make_sz_sz_correlators


BN(x::Integer) = Formatting.format(Int(x), commas=true)



function diagonalize(;hamiltonian_sparse, num_eigens)
  matrix_size = size(hamiltonian_sparse, 1)

  if matrix_size <= max(1024, num_eigens)
    log("Diagonalizing dense Hamiltonian (size = $(BN(matrix_size)))")
    (eigenvalues, eigenvectors) = eigen(Hermitian(Matrix(hamiltonian_sparse)))
    idx = sortperm(eigenvalues)[1:min(matrix_size, num_eigens)]
    eigenvalues = eigenvalues[idx]
    eigenvectors = eigenvectors[:, idx]
    return (eigenvalues, eigenvectors)
  else
    log("Diagonalizing sparse Hamiltonian (size = $(BN(matrix_size)))")
    (eigenvalues, eigenvectors) = eigs(hamiltonian_sparse; which=:SR, nev=num_eigens)
    eigenvalues = real.(eigenvalues)
    idx = sortperm(eigenvalues)
    eigenvalues = eigenvalues[idx]
    eigenvectors = eigenvectors[:, idx]
    return (eigenvalues, eigenvectors)
  end
end


function save(;filepath, groupname, parameters, eigenvalues, eigenvectors, observables, observable_expectations, save_eigenvectors)
  dirpath = dirname(filepath)
  if !ispath(dirpath)
    log("Creating directory $dirpath")
    mkpath(dirpath; mode=0o755)
  elseif !isdir(dirpath)
    @error "$dirpath exists but is not a directory."
  end

  h5open(filepath, "w") do file
    g = g_create(file, groupname)

    attrs(g)["shape"] = parameters["size_matrix"]
    attrs(g)["J1"] = parameters["J1"]
    attrs(g)["J2"] = parameters["J2"]
    attrs(g)["Jc"] = parameters["Jc"]
    attrs(g)["theta"] = parameters["theta"]
    attrs(g)["momentum_index"] = parameters["momentum_index"]
    attrs(g)["twoSz"] = parameters["twoSz"]

    g["eigenvalues"] = eigenvalues
    if save_eigenvectors
      log( "Saving eigenvectors")
      g["eigenvectors", "chunk", (size(eigenvectors, 1), 1), "compress", 9] = eigenvectors
    end
    log("Saving observables")
    g_obs = g_create(g, "observables")
    for ((name, operator, isreal), Oexp) in zip(observables, observable_expectations)
      log("  - $(name)")
      g_obs[name] = Oexp
    end
  end # h5open
end


function measure_observables(;observables, observable_sparses, eigenvectors)
  neig = size(eigenvectors, 2)

  observable_expectations = []
  for (iobs, ((name, operator, isreal), operator_sparse)) in enumerate(zip(observables, observable_sparses))
    log("  Observable $iobs: $name")
    O = [(dot(eigenvectors[:,i], operator_sparse * eigenvectors[:,i])) for i in 1:neig]
    GC.gc()
    if isreal
      push!(observable_expectations, real.(O))
    else
      push!(observable_expectations, O)
    end
  end
  return observable_expectations
end


function run(;size_matrix ::AbstractMatrix{<:Integer},
             allowed_quantum_numbers ::AbstractVector{<:Integer},
             allowed_momentum_indices ::AbstractVector{<:AbstractVector{<:Integer}},
             J1s ::AbstractVector{<:Real},
             J2s ::AbstractVector{<:Real},
             Jcs ::AbstractVector{<:Real},
             thetas ::AbstractVector{<:AbstractVector{<:Real}},
             num_eigens ::Integer,
             observable_names ::AbstractVector{<:AbstractString},
             save_eigenvectors ::Bool,
             use_sparse_matrix ::Bool,
             use_dict ::Bool)

  log("Starting run")

  # STEP 1. Initialization
  shape_string = Formatting.format("({:d},{:d})x({:d},{:d})", size_matrix[1,1], size_matrix[2,1], size_matrix[1,2], size_matrix[2,2])

  log("Making lattice of size ($shape_string)")
  lattice = make_lattice(;size_matrix=size_matrix)
  momentum_shape = [g.order for g in lattice.translation_group.generators]

  log("Making nearest neighbor pairs, second nearest neighbor pairs, and chiral triplets")
  bonds = make_bonds(;hypercube=lattice.hypercube)

  log("Making Hilbertspace")
  hilbert_space = make_hilbert_space(;hypercube=lattice.hypercube)

  log("Making observables")
  observables = NamedTuple{(:name, :operator, :isreal), Tuple{String, AbstractOperator, Bool}}[]
  if "spin_squared" in observable_names
    log("  - Making spin-squared")
    spin_squared = make_spin_squared(;hypercube=lattice.hypercube,
                                     hilbert_space=hilbert_space)
    push!(observables, (name="spin_squared", operator=spin_squared, isreal=true))
  end

  if "sz_sz_correlators" in observable_names
    log("  - Making SzSz correlators")
    sz_sz_correlators = make_sz_sz_correlators(;hypercube=lattice.hypercube,
                                               hilbert_space=hilbert_space)
    for (d1, d2) in lattice.hypercube.coordinates
      push!(observables, (name="sz_sz_correlator[$d1,$d2]", operator=sz_sz_correlators[d1,d2], isreal=true))
    end
  end

  log("Checking whether every observable is translationally invariant")
  for (name, operator, isreal) in observables
    log("  - $name")
    @assert is_invariant(hilbert_space, lattice.translation_group, operator)
  end

  log("Computing quantum number sectors")
  complete_quantum_numbers = quantum_number_sectors(hilbert_space)
  log("  Possible sectors: $complete_quantum_numbers")

  sectors = sort(intersect(complete_quantum_numbers, allowed_quantum_numbers))
  log("  Allowed sectors: $sectors")

  log("Translation group")
  log("  Generators:")
  for ig in get_generators(lattice.hypercube)
    log("    - $(lattice.hypercube.coordinates[ig])")
  end

  log("  Possible momenta: $(lattice.translation_group.fractional_momenta)")

  momenta = if isempty(allowed_momentum_indices)
    lattice.translation_group.fractional_momenta
  else
    allowed_fractional_momenta = [ [k//n for (k, n) in zip(kf, momentum_shape)] for kf in allowed_momentum_indices ]
    sort(intersect(lattice.translation_group.fractional_momenta, allowed_fractional_momenta))
  end
  log("  Allowed momenta: $momenta")

  jobs = Dict()

  for twoSz in sectors, kf in momenta
    jobs[twoSz, kf] = Set()

    momentum_index = [Int(k*n) for (k, n) in zip(kf, momentum_shape)]
    momentum_index_string = join(momentum_index, ",")
    groupname = Formatting.format("{:d}_{:s}", twoSz, momentum_index_string)
    filename = "spectrum_" * groupname * ".hdf5"

    for theta in thetas
      for J1 in J1s, J2 in J2s, Jc in Jcs
        directory_name = Formatting.format("J1={:.3f}_J2={:.3f}_Jc={:.3f}_theta=({:.3f},{:.3f}).qed", J1, J2, Jc, theta[1], theta[2])
        filepath = joinpath(directory_name, filename)
        if !isfile(filepath)
          push!(jobs[twoSz, kf], theta)
        end
      end # for J1, J2, Jc
    end # for theta
  end # for twoSz, kf

  for twoSz in sectors, kf in momenta
    if isempty(jobs[twoSz, kf])
      log("No job in $twoSz, $kf")
      continue
    end
    log("Starting 2Sz = $twoSz, momentum = $kf")

    hilbert_space_representation = if use_dict
      log("Making Hilbert space representation with dict")
      represent_dict(HilbertSpaceSector(hilbert_space, twoSz))
    else
      log("Making Hilbert space representation with sorted array")
      represent(HilbertSpaceSector(hilbert_space, twoSz))
    end

    log("Garbage collection starting.")
    GC.gc()
    log("Garbage collection finished.")

    momentum_index = [Int(k*n) for (k, n) in zip(kf, momentum_shape)]
    momentum_index_string = join(momentum_index, ",")
    groupname = Formatting.format("{:d}_{:s}", twoSz, momentum_index_string)
    filename = "spectrum_" * groupname * ".hdf5"

    log("Working in momentum $(momentum_index) / $(momentum_shape)")

    log("Making reduced_hilbert_space_representation")
    reduced_hilbert_space_representation = symmetry_reduce_parallel(hilbert_space_representation, lattice.translation_group, kf)

    log("Garbage collection starting.")
    GC.gc()
    log("Garbage collection finished.")

    log("Representing observables")
    observable_sparses = AbstractMatrix[]
    for (name, operator, isreal) in observables
      log("  - $name")
      observable_sparse = represent(reduced_hilbert_space_representation, operator)
      push!(observable_sparses, observable_sparse)
    end

    for theta in thetas
      if !(theta in jobs[twoSz, kf])
        log("No job with theta = $theta")
        continue
      end
      log("Starting θ = $theta")
      log("Making Hamiltonian terms.")
      hamiltonian_terms = make_hamiltonian_terms(;
                              hypercube=lattice.hypercube,
                              nearest_neighbor_pairs=bonds.nearest_neighbor_pairs,
                              second_nearest_neighbor_pairs=bonds.second_nearest_neighbor_pairs,
                              chiral_triplets=bonds.chiral_triplets,
                              hilbert_space=hilbert_space,
                              theta=theta,
                          )
      repterm = if use_sparse_matrix
        (x) -> sparse(represent(reduced_hilbert_space_representation, x))
      else
        (x) -> represent(reduced_hilbert_space_representation, x)
      end
      log("Representing j1.")
      j1_sparse = repterm(hamiltonian_terms.j1)
      log("Representing j2.")
      j2_sparse = repterm(hamiltonian_terms.j2)
      log("Representing jc.")
      jc_sparse = repterm(hamiltonian_terms.jc)

      log("Garbage collection starting.")
      GC.gc()
      log("Garbage collection finished.")

      for J1 in J1s, J2 in J2s, Jc in Jcs
        log("Starting J₁ = $J1, J₂ = $J2, Jᵪ = $Jc")
        directory_name = Formatting.format("J1={:.3f}_J2={:.3f}_Jc={:.3f}_theta=({:.3f},{:.3f}).qed", J1, J2, Jc, theta[1], theta[2])
        filepath = joinpath(directory_name, filename)

        if isfile(filepath)
          @warn "$filepath exists. Skipping"
          continue
        end

        log("Summing the terms to make Hamiltonian.")
        hamiltonian_sparse = J1*j1_sparse + J2*j2_sparse + Jc*jc_sparse

        log("Garbage collection starting.")
        GC.gc()
        log("Garbage collection finished.")

        log("Memory RSS: $(BN(get_rss()))")

        (eigenvalues, eigenvectors) = diagonalize(;hamiltonian_sparse=hamiltonian_sparse, num_eigens=num_eigens)

        log("Garbage collection starting.")
        GC.gc()
        log("Garbage collection finished.")

        log("Computing $(length(observables)) observables")
        observable_expectations = measure_observables(observables=observables,
                                                      observable_sparses=observable_sparses,
                                                      eigenvectors=eigenvectors)

        parameters = Dict("size_matrix"=>size_matrix,
                          "J1"=>J1,
                          "J2"=>J2,
                          "Jc"=>Jc,
                          "theta"=>theta,
                          "momentum_index"=>momentum_index,
                          "twoSz"=>twoSz)

        log("Saving $filename")
        save(;filepath=filepath,
              groupname=groupname,
              parameters=parameters,
              eigenvalues=eigenvalues,
              eigenvectors=eigenvectors,
              observables=observables,
              observable_expectations=observable_expectations,
              save_eigenvectors=save_eigenvectors)
      end # for J1 J2 Jc
    end # for theta
  end # for twoSz, kf
end


function parse_commandline()
  s = ArgParseSettings()
  @add_arg_table s begin
    "--shape"
      arg_type = Int
      nargs = 4
      required = true
    "--J1"
      arg_type = Float64
      nargs = '+'
      required = true
    "--J2"
      arg_type = Float64
      nargs = '+'
      required = true
    "--Jc"
      arg_type = Float64
      nargs = '+'
      required = true
    "--twoSz"
      arg_type = Int
      nargs = '+'
      required = true
    "--theta1"
      arg_type = Float64
      nargs = '+'
      required = true
    "--theta2"
      arg_type = Float64
      nargs = '+'
      required = true
    "--num_eigens"
      arg_type = Int
      default = 16
    "--momentum"
      arg_type = String
      nargs = '*'
    "--observables"
      arg_type = String
      nargs = '+'
    "--save_eigenvectors"
      action = :store_true
    "--use_sparse_matrix"
      action = :store_true
    "--use_dict"
      action = :store_true
    "--debug"
      action = :store_true
  end
  return parse_args(s)
end


function main()
  args = parse_commandline()

  if args["debug"]
    logger = ConsoleLogger(stdout, Logging.Debug; meta_formatter=my_metafmt)
    global_logger(logger)
    #Memento.config!(ed_logger, "debug"; fmt="[{date} | {level} | {name}]: {msg}")
  else
    logger = ConsoleLogger(stdout, Logging.Info; meta_formatter=my_metafmt)
    global_logger(logger)
    #Memento.config!(ed_logger, "info"; fmt="[{date} | {level} | {name}]: {msg}")
  end
  log("Number of threads = $(Threads.nthreads())")

  shape = args["shape"]
  size_matrix ::Matrix{Int} = [shape[1] shape[3];
                               shape[2] shape[4]]

  J1s = args["J1"]
  J2s = args["J2"]
  Jcs = args["Jc"]
  twoSzs = args["twoSz"]
  thetas = [[theta1, theta2] for theta1 in args["theta1"] for theta2 in args["theta2"]]
  num_eigens = args["num_eigens"]
  observable_names = args["observables"]

  conv(s ::AbstractString) = Int[parse(Int, t) for t in split(s, ",")]
  allowed_momentum_indices = isempty(args["momentum"]) ? Vector{Int}[] : Vector{Int}[conv(momentum) for momentum in args["momentum"]]

  supported_observables = ["sz_sz_correlators", "spin_squared"]
  @assert isempty(setdiff(observable_names, supported_observables))

  save_eigenvectors = args["save_eigenvectors"]
  use_sparse_matrix = args["use_sparse_matrix"]
  use_dict = args["use_dict"]

  run(size_matrix=size_matrix,
      allowed_quantum_numbers=twoSzs,
      allowed_momentum_indices=allowed_momentum_indices,
      J1s=J1s, J2s=J2s, Jcs=Jcs,
      thetas=thetas,
      num_eigens=num_eigens,
      observable_names=observable_names,
      save_eigenvectors=save_eigenvectors,
      use_sparse_matrix=use_sparse_matrix,
      use_dict=use_dict)

end # main


main()
