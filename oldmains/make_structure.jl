using LinearAlgebra
using SparseArrays
using Arpack
using JLD2
using ExactDiagonalization
using ArgParse
using Memento

main_logger = Memento.config!("info"; fmt="[{date} | {level} | {name}]: {msg}")

include("src/SquareJ1J2J3.jl")

function parse_commandline()
  s = ArgParseSettings()
  @add_arg_table s begin
    "--n1"
      arg_type = Int
      required = true
    "--n2"
      arg_type = Int
      required = true
    "--sz"
      arg_type = Int
      required = true
  end
  return parse_args(s)
end

function main()
  args = parse_commandline()

  n1 = args["n1"]
  n2 = args["n2"]
  qn = args["sz"]

  info(main_logger, "Number of threads = $(Threads.nthreads())")
  info(main_logger, "Parameteters : n1 = $n1, n2 = $n2, qn = $qn")

  info(main_logger, "Making J1, J2, J3 terms")
  info(main_logger, "Sector = $qn")

  structure_filename = "structure_$(n1)_$(n2)_$(qn).jld2"

  hs = nothing
  j1_sparse = nothing
  j2_sparse = nothing
  j3_sparse = nothing
  σxsq_sparse = nothing
  σysq_sparse = nothing
  σzsq_sparse = nothing
  σsq_sparse = nothing

  (hs, j1, j2, j3, total_ssq) = make_J1J2J3_hamiltonian(n1, n2)

  info(main_logger, "Concretizing Hilbert Space"); flush(stdout)
  chs = concretize(hs, Set([qn]))
  info(main_logger, "Hilbert space dimension = $(length(chs.basis_list))")

  info(main_logger, "Materializing J1"); flush(stdout)
  j1_sparse, ϵ = materialize_parallel(chs, j1)
  info(main_logger, "Number of nonzero values = $(length(j1_sparse.nzval))")
  info(main_logger, "Bound for error = $ϵ")

  info(main_logger, "Materializing J2"); flush(stdout)
  j2_sparse, ϵ = materialize_parallel(chs, j2)
  info(main_logger, "Number of nonzero values = $(length(j2_sparse.nzval))")
  info(main_logger, "Bound for error = $ϵ")

  info(main_logger, "Materializing J3"); flush(stdout)
  j3_sparse, ϵ = materialize_parallel(chs, j3)
  info(main_logger, "Number of nonzero values = $(length(j3_sparse.nzval))")
  info(main_logger, "Bound for error = $ϵ")

  info(main_logger, "Materializing spin"); flush(stdout)
  σsq_sparse, ϵ = materialize_parallel(chs, total_ssq)
  info(main_logger, "Number of nonzero values = $(length(σxsq_sparse.nzval))")
  info(main_logger, "Bound for error = $ϵ")

  @save structure_filename hs chs j1_sparse j2_sparse j3_sparse σsq_sparse
end

main()
