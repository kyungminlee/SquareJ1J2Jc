using LinearAlgebra
using SparseArrays
using Arpack
using JLD2
using ExactDiagonalization
using ArgParse
using Memento

main_logger = Memento.config!("info"; fmt="[{date} | {level} | {name}]: {msg}")

include("src/SquareJ1J2J3.jl")

function parse_commandline()
  s = ArgParseSettings()
  @add_arg_table s begin
    "--n1"
      arg_type = Int
      required = true
    "--n2"
      arg_type = Int
      required = true
    "--sz"
      arg_type = Int
      required = true
    "--J2"
      arg_type = Float64
      nargs = '+'
    "--J3"
      arg_type = Float64
      nargs = '+'
  end
  return parse_args(s)
end

function main()
  args = parse_commandline()

  n1 = args["n1"]
  n2 = args["n2"]
  qn = args["sz"]
  J2s = args["J2"]
  J3s = args["J3"]

  info(main_logger, "Number of threads = $(Threads.nthreads())")
  info(main_logger, "Parameteters : n1 = $n1, n2 = $n2, sz = $qn")
  
  info(main_logger, "Making J1, J2, J3 terms")
  info(main_logger, "Sector = $qn")

  (hs, j1, j2, j3, total_ssq) = make_J1J2J3_hamiltonian(n1, n2)

  info(main_logger, "Concretizing Hilbert Space"); flush(stdout)
  chs = concretize(hs, Set([qn]))
  info(main_logger, "Hilbert space dimension = $(length(chs.basis_list))")

  info(main_logger, "Materializing J1"); flush(stdout)
  j1_sparse, ϵ = materialize_parallel(chs, j1)
  info(main_logger, "Number of nonzero values = $(length(j1_sparse.nzval))")
  info(main_logger, "Bound for error = $ϵ")

  info(main_logger, "Materializing J2"); flush(stdout)
  j2_sparse, ϵ = materialize_parallel(chs, j2)
  info(main_logger, "Number of nonzero values = $(length(j2_sparse.nzval))")
  info(main_logger, "Bound for error = $ϵ")

  info(main_logger, "Materializing J3"); flush(stdout)
  j3_sparse, ϵ = materialize_parallel(chs, j3)
  info(main_logger, "Number of nonzero values = $(length(j3_sparse.nzval))")
  info(main_logger, "Bound for error = $ϵ")

  info(main_logger, "Materializing S^2"); flush(stdout)
  σsq_sparse, ϵ = materialize_parallel(chs, total_ssq)
  info(main_logger, "Number of nonzero values = $(length(σsq_sparse.nzval))")
  info(main_logger, "Bound for error = $ϵ")

  #structure_filename = "structure_$(n1)_$(n2)_$(qn).jld2"

  #info(main_logger, "Loading $(structure_filename)"); flush(stdout)
  #@load structure_filename hs chs j1_sparse j2_sparse j3_sparse σsq_sparse
  #info(main_logger, "Successful"); flush(stdout)

  J1 = 1.0
  for J2 in J2s, J3 in J3s
    info(main_logger, "Starting (J1, J2, J3) = ($J1, $J2, $J3)")
    info(main_logger, "Constructing the total Hamiltonian")
    H = J1 * j1_sparse + J2 * j2_sparse + J3 * j3_sparse

    filename = "spectrum_$(n1)_$(n2)_$(qn)_$(J1)_$(J2)_$(J3).jld2"
    eigenvalues = nothing
    eigenvectors = nothing
    total_spin = nothing
    if size(H)[1] <= 200
      info(main_logger, "Diagonalizing dense Hamiltonian (size = $(size(H)[1]))")
      (eigenvalues, eigenvectors) = eigen(Hermitian(Matrix(H)))
      idx = sortperm(eigenvalues)
      eigenvalues = eigenvalues[idx]
      eigenvectors = eigenvectors[:, idx]
      neig = length(eigenvalues)
      total_spin = [dot(eigenvectors[:,i], σsq_sparse * eigenvectors[:,i]) for i in 1:neig]
    else
      info(main_logger, "Diagonalizing sparse Hamiltonian (size = $(size(H)[1]))")
      (eigenvalues, eigenvectors) = eigs(H; which=:SR, nev=36)
      eigenvalues = real.(eigenvalues)
      idx = sortperm(eigenvalues)
      eigenvalues = eigenvalues[idx]
      eigenvectors = eigenvectors[:, idx]
      neig = length(eigenvalues)
      total_spin = [dot(eigenvectors[:,i], σsq_sparse * eigenvectors[:,i]) for i in 1:neig]
    end
    info(main_logger, "Saving $filename")
    @save filename n1 n2 qn J1 J2 J3 eigenvalues eigenvectors total_spin
  end # for J2, J3

end

main()
