using LinearAlgebra
using SparseArrays

using Arpack
using JLD2
using FileIO
using ArgParse
using Memento
import Formatting
using DataStructures
using JSON
using HDF5
using Glob
using Parameters

using ExactDiagonalization
using TightBindingLattice


main_logger = Memento.config!("info"; fmt="[{date} | {level} | {name}]: {msg}")

include("src/SquareJ1J2Jc.jl")

config = Dict(
#"database_directory" => "C:\\Users\\kyungminlee\\Dropbox\\Code\\SquareJ1J2Jc\\Database"
"database_directory" => joinpath(pwd(), "database")
#raw"C:\Users\kyungminlee\Projects\SquareJ1J2Jc\Result-Type3\Database"
)

using .SquareJ1J2Jc: make_lattice,
                     make_bonds,
                     make_hilbert_space,
                     make_hamiltonian_terms,
                     make_spin_squared,
                     make_sz_sz_correlators

@with_kw struct S12cParameter
  shape ::Matrix{Int} = [1 0; 0 1]
  J1::Float64 = 1.0
  J2::Float64 = 0.0
  Jc::Float64 = 0.0
  theta::Vector{Float64} = [0.0, 0.0]
end


function parse_commandline()
  s = ArgParseSettings()
  @add_arg_table s begin
    "directories"
      arg_type = String
      nargs = '+'
  end
  return parse_args(s)
end

function pushd(f::Function, dir ::AbstractString)
  current_dir = pwd()
  real_dir = realpath(dir)
  try
    cd(real_dir)
    f()
  finally
    cd(current_dir)
  end
end

BN(x::Integer) = Formatting.format(x, commas=true)


function read_or_runsave(f::Function, filepath::AbstractString, name::AbstractString)
  if isfile(filepath)
    info(main_logger, "File $(filepath) exists.")

    existing_names = jldopen(filepath, "r") do file
      keys(file)
    end

    if name in existing_names
      info(main_logger, "  Item $(name) exists in $(filepath). Reading.")
      load(filepath, name)
    else
      info(main_logger, "  Item $(name) does not exist in $(filepath).")

      result = f()
      info(main_logger, "Saving $(name) to $(filepath)")
      jldopen(filepath, "r+", compress=true) do file
        write(file, name, result)
      end
      result
    end
  else
    info(main_logger, "File $(filepath) does not exist.")

    result = f()
    info(main_logger, "Saving $(name) to $(filepath)")
    jldopen(filepath, "w", compress=true) do file
      write(file, name, result)
    end
    result
  end
end
#
# mutable struct LazyCache
#   object ::Any
#   filepath ::String
#   object_names ::Vector{String}
#   generator ::Function
#   LazyCache(generator ::Function, filepath:: AbstractString, object_name::AbstractString) = new(nothing, filepath, [object_name], generator)
#   LazyCache(generator ::Function, filepath:: AbstractString, object_names::AbstractVector{<:AbstractString}) = new(nothing, filepath, object_names, generator)
# end
#
# import Base.getindex
# function Base.getindex(lc ::LazyCache, ::Tuple{})
#   if lc.object === nothing
#     try
#       if isfile(lc.filepath)
#         lc.object = READ
#       end
#     catch
#       # do nothing
#     end
#
#     if lc.object === nothing
#       object = generator()
#       SAVE object
#     end
#   else
#     return lc.object
#   end
# end


function read_or_runsave(f::Function,
                         filepath::AbstractString,
                         object_names::Union{NTuple{N, <:AbstractString}, AbstractVector{<:AbstractString}}) where N
  if isfile(filepath)
    info(main_logger, "File $(filepath) exists.")

    existing_names = jldopen(filepath, "r") do file
      keys(file)
    end

    results = []
    for name in object_names
      if name in existing_names
        info(main_logger, "  Item $(name) exists in $(filepath). Reading.")
        result = load(filepath, name)
        push!(results, result)
      else
        info(main_logger, "  Item $(name) does not exist in $(filepath).")

        result = f()
        info(main_logger, "Saving $(name) to $(filepath)")
        jldopen(filepath, "r+", compress=true) do file
          write(file, name, result)
        end
        push!(results, result)
      end
    end
    (results...,)
  else
    info(main_logger, "File $(filepath) does not exist.")

    results = f()
    @assert length(results) == length(object_names)

    info(main_logger, "Saving $(object_names) to $(filepath)")
    jldopen(filepath, "w", compress=true) do file
      for (name, result) in zip(object_names, results)
        write(file, name, result)
      end
    end
    (results...,)
  end
end



function run(;size_matrix ::AbstractMatrix{<:Integer},
             allowed_quantum_numbers ::AbstractVector{<:Integer},
             J1 ::Real, J2 ::Real, Jc ::Real,
             theta ::AbstractVector{<:Real},
             num_eigens ::Integer,
             observable_names ::AbstractVector{<:AbstractString},
             save_eigenvectors ::Bool)

  shape_string = Formatting.format("({:d},{:d})x({:d},{:d})", size_matrix[1,1], size_matrix[2,1], size_matrix[1,2], size_matrix[2,2])
  #hypercube = HypercubicLattice(size_matrix)
  #info(main_logger, "Parameteters : shape = $size_matrix, 2Sz = $(twoSz), θ = $(theta) (in units of 2π)")


  info(main_logger, "Making lattice of size ($shape_string)")
  lattice = make_lattice(;size_matrix=size_matrix)


  info(main_logger, "Making nearest neighbor pairs, second nearest neighbor pairs, and chiral triplets")
  bonds = make_bonds(;hypercube=lattice.hypercube)


  info(main_logger, "Making Hilbertspace")
  hilbert_space = make_hilbert_space(;hypercube=lattice.hypercube)


  info(main_logger, "Making observables")
  observables = NamedTuple{(:name, :operator, :isreal), Tuple{String, AbstractOperator, Bool}}[]
  if "spin_squared" in observable_names
    info(main_logger, "  - Making spin-squared")
    spin_squared = make_spin_squared(;hypercube=lattice.hypercube,
                                     hilbert_space=hilbert_space)
    push!(observables, (name="spin_squared", operator=spin_squared, isreal=true))
  end
  if "sz_sz_correlators" in observable_names
    info(main_logger, "  - Making SzSz correlators")
    sz_sz_correlators = make_sz_sz_correlators(;hypercube=lattice.hypercube,
                                               hilbert_space=hilbert_space)
    for (d1, d2) in lattice.hypercube.coordinates
      push!(observables, (name="sz_sz_correlator[$d1,$d2]", operator=sz_sz_correlators[d1,d2], isreal=true))
    end
  end


  info(main_logger, "Checking whether every observable is translationally invariant")
  for (name, operator, isreal) in observables
    info(main_logger, "  - $name")
    @assert is_invariant(hilbert_space, lattice.translation_group, operator)
  end


  info(main_logger, "Computing quantum number sectors")
  complete_quantum_numbers = quantum_number_sectors(hilbert_space)
  info(main_logger, "  Possible sectors are: $complete_quantum_numbers")

  sectors = sort(intersect(complete_quantum_numbers, allowed_quantum_numbers))
  info(main_logger, "  Allowed sectors are: $sectors")

  database_directory ::String = config["database_directory"]
  if !ispath(database_directory)
    info(main_logger, "$database_directory does not exist. Creating.")
    mkpath(database_directory, mode=0o755)
  elseif !isdir(database_directory)
    error(main_logger, "$database_directory exists but is not a directory.")
  end

  for twoSz in sectors
    representation_filename = "representation_shape=$(shape_string),twoSz=$(twoSz).jld2"
    representation_filepath = joinpath(database_directory, representation_filename)

    # LOAD OR MAKE: Reduced Hilbert space representation
    hilbert_space_representation = nothing

    for kf in lattice.translation_group.fractional_momenta
      momentum_shape = [g.order for g in lattice.translation_group.generators]
      momentum_index = [Int(k*n) for (k, n) in zip(kf, momentum_shape)]
      momentum_index_string = join(momentum_index, ",")
      theta_string = Formatting.format("{:.3f},{:.3f}", theta[1], theta[2])

      reduced_representation_filename = "reduced_representation_shape=$(shape_string),twoSz=$(twoSz),k=($(momentum_index_string)).jld2"
      reduced_observables_filename = "reduced_operator_shape=$(shape_string),twoSz=$(twoSz),k=($(momentum_index_string)).jld2"
      reduced_hamiltonian_filename = "reduced_hamiltonian_shape=$(shape_string),twoSz=$(twoSz),k=($(momentum_index_string)),theta=($(theta_string)).jld2"

      reduced_representation_filepath = joinpath(database_directory, reduced_representation_filename)
      reduced_observables_filepath    = joinpath(database_directory, reduced_observables_filename)
      reduced_hamiltonian_filepath    = joinpath(database_directory, reduced_hamiltonian_filename)

      info(main_logger, "Working in momentum $(momentum_index) / $(momentum_shape)")

      # LOAD OR MAKE: Reduced Hilbert space representation
      reduced_hilbert_space_representation = nothing

      observable_sparses = AbstractSparseArray[]
      for (name, operator, isreal) in observables

        observable_sparse = read_or_runsave(reduced_observables_filepath, name) do

          if reduced_hilbert_space_representation === nothing
            reduced_hilbert_space_representation = read_or_runsave(reduced_representation_filepath, "reduced_hilbert_space_representation") do
              if hilbert_space_representation === nothing
                hilbert_space_representation = read_or_runsave(representation_filepath, "hilbert_space_representation") do
                  info(main_logger, "Making Hilbert space representation")
                  represent_dict(HilbertSpaceSector(hilbert_space, twoSz))
                end
              end

              info(main_logger, "Making reduced_hilbert_space_representation")
              symmetry_reduce_parallel(hilbert_space_representation, lattice.translation_group, kf)
            end

            if hilbert_space_representation === nothing
              hilbert_space_representation = reduced_hilbert_space_representation.parent
            end
          end

          sparse(represent(reduced_hilbert_space_representation, operator))
        end
        push!(observable_sparses, observable_sparse)
      end


      (j1_sparse, j2_sparse, jc_sparse) = read_or_runsave(reduced_hamiltonian_filepath, ("j1", "j2", "jc")) do

        if reduced_hilbert_space_representation === nothing
          reduced_hilbert_space_representation = read_or_runsave(reduced_representation_filepath, "reduced_hilbert_space_representation") do
            if hilbert_space_representation === nothing
              hilbert_space_representation = read_or_runsave(representation_filepath, "hilbert_space_representation") do
                info(main_logger, "Making Hilbert space representation")
                represent_dict(HilbertSpaceSector(hilbert_space, twoSz))
              end
            end

            info(main_logger, "Making reduced_hilbert_space_representation")
            symmetry_reduce_parallel(hilbert_space_representation, lattice.translation_group, kf)
          end

          if hilbert_space_representation === nothing
            hilbert_space_representation = reduced_hilbert_space_representation.parent
          end
        end

        info(main_logger, "Making Hamiltonian terms.")
        hamiltonian_terms = make_hamiltonian_terms(;
                                hypercube=lattice.hypercube,
                                nearest_neighbor_pairs=bonds.nearest_neighbor_pairs,
                                second_nearest_neighbor_pairs=bonds.second_nearest_neighbor_pairs,
                                chiral_triplets=bonds.chiral_triplets,
                                hilbert_space=hilbert_space,
                                theta=theta,
                            )
        info(main_logger, "Representing j1.")
        j1_sparse = sparse(represent(reduced_hilbert_space_representation, hamiltonian_terms.j1))
        info(main_logger, "Representing j2.")
        j2_sparse = sparse(represent(reduced_hilbert_space_representation, hamiltonian_terms.j2))
        info(main_logger, "Representing jc.")
        jc_sparse = sparse(represent(reduced_hilbert_space_representation, hamiltonian_terms.jc))
        (j1_sparse, j2_sparse, jc_sparse)
      end # if isfile(reduced_hamiltonian_filepath)

      info(main_logger, "Summing the terms to make Hamiltonian.")

      hamiltonian_sparse = J1*j1_sparse + J2*j2_sparse + Jc*jc_sparse
      @assert all(size(op) == size(hamiltonian_sparse) for op in observable_sparses)

      info(main_logger, "Garbage collection starting.")
      GC.gc()
      info(main_logger, "Garbage collection finished.")

      eigenvalues = nothing
      eigenvectors = nothing
      observable_expectations = []
      matrix_size = size(hamiltonian_sparse, 1)

      if matrix_size <= max(400, num_eigens)
        info(main_logger, "Diagonalizing dense Hamiltonian (size = $(BN(matrix_size)))")
        (eigenvalues, eigenvectors) = eigen(Hermitian(Matrix(hamiltonian_sparse)))
        idx = sortperm(eigenvalues)[1:min(matrix_size, num_eigens)]
        eigenvalues = eigenvalues[idx]
        eigenvectors = eigenvectors[:, idx]
      else
        info(main_logger, "Diagonalizing sparse Hamiltonian (size = $(BN(matrix_size)))")
        (eigenvalues, eigenvectors) = eigs(hamiltonian_sparse; which=:SR, nev=num_eigens)
        eigenvalues = real.(eigenvalues)
        idx = sortperm(eigenvalues)
        eigenvalues = eigenvalues[idx]
        eigenvectors = eigenvectors[:, idx]
      end
      neig = length(eigenvalues)

      if !isempty(observables)
        info(main_logger, "Computing $(length(observables)) observables")
        for (iobs, ((name, operator, isreal), operator_sparse)) in enumerate(zip(observables, observable_sparses))
          info(main_logger, "  Observable $iobs: $name")
          O = [(dot(eigenvectors[:,i], operator_sparse * eigenvectors[:,i])) for i in 1:neig]
          if isreal
            push!(observable_expectations, real.(O))
          else
            push!(observable_expectations, O)
          end
        end
      end

      groupname = Formatting.format("{:d}_{:s}", twoSz, momentum_index_string)
      filename = "spectrum_" * groupname * ".hdf5"

      info(main_logger, "Saving $filename")
      h5open(filename, "w") do file
        g = g_create(file, groupname)

        attrs(g)["shape"] = size_matrix
        attrs(g)["J1"] = J1
        attrs(g)["J2"] = J2
        attrs(g)["Jc"] = Jc
        attrs(g)["theta"] = theta
        attrs(g)["momentum_index"] = momentum_index
        attrs(g)["twoSz"] = twoSz

        g["eigenvalues"] = eigenvalues
        if save_eigenvectors
          g["eigenvectors", "chunk", (matrix_size, neig), "compress", 9] = eigenvectors
        end
        g_obs = g_create(g, "observables")
        for ((name, operator, isreal), Oexp) in zip(observables, observable_expectations)
          g_obs[name] = Oexp
        end
      end # h5open

    end # for kf

  end # for twoSz
end

function main()
  args = parse_commandline()

  info(main_logger, "Number of threads = $(Threads.nthreads())")
  directories ::Vector{String} = if Sys.iswindows()
    vcat([Glob.glob(d) for d in args["directories"]]...)
  else
    args["directories"]
  end

  for directory in directories

    info(main_logger, "Processing $directory")
    pushd(directory) do
      isfile("finished") && return

      parameters = JSON.parsefile("parameters.json")

      shape = parameters["shape"]
      size_matrix ::Matrix{Int} = hcat(shape...)
      @assert size(size_matrix) == (2, 2)
      parameters["shape"] = transpose(size_matrix)

      J1 ::Float64, J2 ::Float64, Jc ::Float64 = parameters["J1"], parameters["J2"], parameters["Jc"]
      theta ::Vector{Float64} = parameters["theta"]
      num_eigens ::Int = parameters["num_eigens"]
      observable_names ::Vector{String} = parameters["observables"]
      save_eigenvectors ::Bool = parameters["save_eigenvectors"]

      run(size_matrix=size_matrix,
          allowed_quantum_numbers=0:8,
          J1=J1, J2=J2, Jc=Jc,
          theta=theta,
          num_eigens=num_eigens,
          observable_names=observable_names,
          save_eigenvectors=save_eigenvectors)

      touch("finished")
    end # pushd

  end # for directory

end # main

main()
