using LinearAlgebra
using SparseArrays

using Arpack
using JLD2
using FileIO
using ArgParse
using Memento
import Formatting
using DataStructures
using JSON
using HDF5
using Glob
using Parameters

using ExactDiagonalization
using TightBindingLattice


main_logger = Memento.config!("info"; fmt="[{date} | {level} | {name}]: {msg}")
log(msg) = info(main_logger, msg)

include("src/SquareJ1J2Jc.jl")

using .SquareJ1J2Jc: make_lattice,
                     make_bonds,
                     make_hilbert_space,
                     make_hamiltonian_terms,
                     make_spin_squared,
                     make_sz_sz_correlators

@with_kw struct S12cParameter
  shape ::Matrix{Int} = [1 0; 0 1]
  theta::Vector{Float64} = [0.0, 0.0]
  J::Vector{Float64} = [1.0, 0.0, 0.0]
end

function pushd(f::Function, dir ::AbstractString)
  current_dir = pwd()
  real_dir = realpath(dir)
  try
    cd(real_dir)
    f()
  finally
    cd(current_dir)
  end
end

BN(x::Integer) = Formatting.format(x, commas=true)


macro parametrize(ps...)
  head = :call
  args = Any[:Dict]
  for p in ps
    name = string(p)
    push!(args, :($name=>$p))
  end
  return Expr(head, args...)
end


function diagonalize(;hamiltonian_sparse, num_eigens)
  matrix_size = size(hamiltonian_sparse, 1)

  if matrix_size <= max(400, num_eigens)
    log("Diagonalizing dense Hamiltonian (size = $(BN(matrix_size)))")
    (eigenvalues, eigenvectors) = eigen(Hermitian(Matrix(hamiltonian_sparse)))
    idx = sortperm(eigenvalues)[1:min(matrix_size, num_eigens)]
    eigenvalues = eigenvalues[idx]
    eigenvectors = eigenvectors[:, idx]
    return (eigenvalues, eigenvectors)
  else
    log("Diagonalizing sparse Hamiltonian (size = $(BN(matrix_size)))")
    (eigenvalues, eigenvectors) = eigs(hamiltonian_sparse; which=:SR, nev=num_eigens)
    eigenvalues = real.(eigenvalues)
    idx = sortperm(eigenvalues)
    eigenvalues = eigenvalues[idx]
    eigenvectors = eigenvectors[:, idx]
    return (eigenvalues, eigenvectors)
  end
end


function save(;filepath, groupname, parameters, eigenvalues, eigenvectors, observables, observable_expectations, save_eigenvectors)
  dirpath = dirname(filepath)
  if !ispath(dirpath)
    log("Creating directory $dirpath")
    mkpath(dirpath; mode=0o755)
  elseif !isdir(dirpath)
    error(main_logger, "$dirpath exists but is not a directory.")
  end

  h5open(filepath, "w") do file
    g = g_create(file, groupname)

    attrs(g)["shape"] = parameters["size_matrix"]
    attrs(g)["J1"] = parameters["J1"]
    attrs(g)["J2"] = parameters["J2"]
    attrs(g)["Jc"] = parameters["Jc"]
    attrs(g)["theta"] = parameters["theta"]
    attrs(g)["momentum_index"] = parameters["momentum_index"]
    attrs(g)["twoSz"] = parameters["twoSz"]

    g["eigenvalues"] = eigenvalues
    if save_eigenvectors
	  log("Saving eigenvectors")
      g["eigenvectors", "chunk", (size(eigenvectors, 1), 1), "compress", 9] = eigenvectors
    end
	log("Saving observables")
    g_obs = g_create(g, "observables")
    for ((name, operator, isreal), Oexp) in zip(observables, observable_expectations)
	  log("  - $(name)")
      g_obs[name] = Oexp
    end
  end # h5open
end


function measure_observables(;observables, observable_sparses, eigenvectors)
  neig = size(eigenvectors, 2)

  observable_expectations = []
  for (iobs, ((name, operator, isreal), operator_sparse)) in enumerate(zip(observables, observable_sparses))
    log("  Observable $iobs: $name")
    O = [(dot(eigenvectors[:,i], operator_sparse * eigenvectors[:,i])) for i in 1:neig]
    if isreal
      push!(observable_expectations, real.(O))
    else
      push!(observable_expectations, O)
    end
  end
  return observable_expectations
end



function run(;size_matrix ::AbstractMatrix{<:Integer},
             allowed_quantum_numbers ::AbstractVector{<:Integer},
             allowed_momentum_indices ::AbstractVector{<:AbstractVector{<:Integer}},
             J1s ::AbstractVector{<:Real},
             J2s ::AbstractVector{<:Real},
             Jcs ::AbstractVector{<:Real},
             thetas ::AbstractVector{<:AbstractVector{<:Real}},
             num_eigens ::Integer,
             observable_names ::AbstractVector{<:AbstractString},
             save_eigenvectors ::Bool,
             periodic::Bool)

  # STEP 1. Initialization
  shape_string = Formatting.format("({:d},{:d})x({:d},{:d})", size_matrix[1,1], size_matrix[2,1], size_matrix[1,2], size_matrix[2,2])

  log("Making lattice of size ($shape_string)")
  lattice = make_lattice(;size_matrix=size_matrix)
  momentum_shape = [g.order for g in lattice.translation_group.generators]

  log("Making nearest neighbor pairs, second nearest neighbor pairs, and chiral triplets")
  bonds = make_bonds(;hypercube=lattice.hypercube, periodic=periodic)
  # println(bonds)

  log("Making Hilbertspace")
  hilbert_space = make_hilbert_space(;hypercube=lattice.hypercube)

  log("Making observables")
  observables = NamedTuple{(:name, :operator, :isreal), Tuple{String, AbstractOperator, Bool}}[]
  if "spin_squared" in observable_names
    log("  - Making spin-squared")
    spin_squared = make_spin_squared(;hypercube=lattice.hypercube,
                                     hilbert_space=hilbert_space)
    push!(observables, (name="spin_squared", operator=spin_squared, isreal=true))
  end

  if "sz_sz_correlators" in observable_names
    log("  - Making SzSz correlators")
    sz_sz_correlators = make_sz_sz_correlators(;hypercube=lattice.hypercube,
                                               hilbert_space=hilbert_space)
    for (d1, d2) in lattice.hypercube.coordinates
      push!(observables, (name="sz_sz_correlator[$d1,$d2]", operator=sz_sz_correlators[d1,d2], isreal=true))
    end
  end

  log("Checking whether every observable is translationally invariant")
  for (name, operator, isreal) in observables
    log("  - $name")
    @assert is_invariant(hilbert_space, lattice.translation_group, operator)
  end

  log("Computing quantum number sectors")
  complete_quantum_numbers = quantum_number_sectors(hilbert_space)
  log("  Possible sectors are: $complete_quantum_numbers")

  sectors = sort(intersect(complete_quantum_numbers, allowed_quantum_numbers))
  log("  Allowed sectors are: $sectors")

  # momenta = if isempty(allowed_momentum_indices)
  #   lattice.translation_group.fractional_momenta
  # else
  #   allowed_fractional_momenta = [ [k//n for (k, n) in zip(kf, momentum_shape)] for kf in allowed_momentum_indices ]
  #   sort(intersect(lattice.translation_group.fractional_momenta, allowed_fractional_momenta))
  # end
  # log("  Allowed momenta are: $momenta")

  #for twoSz in sectors, kf in momenta
  for twoSz in sectors
    log("Making Hilbert space representation")
    hilbert_space_representation = represent_dict(HilbertSpaceSector(hilbert_space, twoSz))

    # momentum_index = [Int(k*n) for (k, n) in zip(kf, momentum_shape)]
    # momentum_index_string = join(momentum_index, ",")
    # groupname = Formatting.format("{:d}_{:s}", twoSz, momentum_index_string)
    # filename = "spectrum_" * groupname * ".hdf5"
    #
    # log("Working in momentum $(momentum_index) / $(momentum_shape)")
    #
    #log("Making reduced_hilbert_space_representation")
    #reduced_hilbert_space_representation = symmetry_reduce_parallel(hilbert_space_representation, lattice.translation_group, kf)
    #
    # log("Representing observables")
    # observable_sparses = AbstractSparseArray[]
    # for (name, operator, isreal) in observables
    #   log("  - $name")
    #   observable_sparse = sparse(represent(reduced_hilbert_space_representation, operator))
    #   push!(observable_sparses, observable_sparse)
    # end
    #
    for theta in thetas
      log("Making Hamiltonian terms.")
      hamiltonian_terms = make_hamiltonian_terms(;
                              hypercube=lattice.hypercube,
                              nearest_neighbor_pairs=bonds.nearest_neighbor_pairs,
                              second_nearest_neighbor_pairs=bonds.second_nearest_neighbor_pairs,
                              chiral_triplets=bonds.chiral_triplets,
                              hilbert_space=hilbert_space,
                              theta=theta,
                          )
      log("Representing j1.")
      j1_sparse = sparse(represent(hilbert_space_representation, hamiltonian_terms.j1))
      log("Representing j2.")
      j2_sparse = sparse(represent(hilbert_space_representation, hamiltonian_terms.j2))
      log("Representing jc.")
      jc_sparse = sparse(represent(hilbert_space_representation, hamiltonian_terms.jc))

      log("Garbage collection starting.")
      GC.gc()
      log("Garbage collection finished.")

      for J1 in J1s, J2 in J2s, Jc in Jcs
        # directory_name = Formatting.format("J1={:.3f}_J2={:.3f}_Jc={:.3f}_theta=({:.3f},{:.3f}).qed", J1, J2, Jc, theta[1], theta[2])
        # filepath = joinpath(directory_name, filename)
        #
        # if isfile(filepath)
        #   warn(main_logger, "$filepath exists. Skipping")
        #   continue
        # end

        log("Summing the terms to make Hamiltonian.")
        hamiltonian_sparse = J1*j1_sparse + J2*j2_sparse + Jc*jc_sparse

        (eigenvalues, eigenvectors) = diagonalize(;hamiltonian_sparse=hamiltonian_sparse, num_eigens=num_eigens)

        # log("Computing $(length(observables)) observables")
        # observable_expectations = measure_observables(observables=observables,
        #                                               observable_sparses=observable_sparses,
        #                                               eigenvectors=eigenvectors)

        # parameters = Dict("size_matrix"=>size_matrix,
        #                   "J1"=>J1,
        #                   "J2"=>J2,
        #                   "Jc"=>Jc,
        #                   "theta"=>theta,
        #                   "momentum_index"=>momentum_index,
        #                   "twoSz"=>twoSz)
        log("Minimum eigenvalue: $(minimum(eigenvalues))")

        # log("Saving $filename")
        # save(;filepath=filepath,
        #       groupname=groupname,
        #       parameters=parameters,
        #       eigenvalues=eigenvalues,
        #       eigenvectors=eigenvectors,
        #       observables=observables,
        #       observable_expectations=observable_expectations,
        #       save_eigenvectors=save_eigenvectors)
      end # for J1 J2 Jc
    end # for theta
  end # for twoSz, kf
end





function parse_commandline()
  s = ArgParseSettings()
  @add_arg_table s begin
    "--shape"
      arg_type = Int
      nargs = 4
      required = true
    "--J1"
      arg_type = Float64
      nargs = '+'
      required = true
    "--J2"
      arg_type = Float64
      nargs = '+'
      required = true
    "--Jc"
      arg_type = Float64
      nargs = '+'
      required = true
    "--twoSz"
      arg_type = Int
      nargs = '+'
      required = true
    "--theta1"
      arg_type = Float64
      nargs = '+'
      required = true
    "--theta2"
      arg_type = Float64
      nargs = '+'
      required = true
    "--num_eigens"
      arg_type = Int
      default = 16
    "--momentum"
      arg_type = String
      nargs = '*'
    "--observables"
      arg_type = String
      nargs = '+'
    "--save_eigenvectors"
      action = :store_true
    "--periodic"
      action = :store_true
  end
  return parse_args(s)
end

function main()
  args = parse_commandline()

  log("Number of threads = $(Threads.nthreads())")

  shape = args["shape"]
  size_matrix ::Matrix{Int} = [shape[1] shape[3];
                               shape[2] shape[4]]

  J1s = args["J1"]
  J2s = args["J2"]
  Jcs = args["Jc"]
  twoSzs = args["twoSz"]
  thetas = [[theta1, theta2] for theta1 in args["theta1"] for theta2 in args["theta2"]]
  num_eigens = args["num_eigens"]
  observable_names = args["observables"]

  conv(s ::AbstractString) = Int[parse(Int, t) for t in split(s, ",")]
  allowed_momentum_indices = isempty(args["momentum"]) ? Vector{Int}[] : Vector{Int}[conv(momentum) for momentum in args["momentum"]]

  supported_observables = ["sz_sz_correlators", "spin_squared"]
  @assert isempty(setdiff(observable_names, supported_observables))

  save_eigenvectors = args["save_eigenvectors"]

  run(size_matrix=size_matrix,
      allowed_quantum_numbers=twoSzs,
      allowed_momentum_indices=allowed_momentum_indices,
      J1s=J1s, J2s=J2s, Jcs=Jcs,
      thetas=thetas,
      num_eigens=num_eigens,
      observable_names=observable_names,
      save_eigenvectors=save_eigenvectors,
      periodic=args["periodic"])
  #touch("finished")

end # main

main()

#julia ..\spectrum_many.jl --shape 4 0 0 4 --J1 1.0 --J2 0.0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0  --Jc 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0 --twoSz 0 --theta1 0.0 0.5 --theta2 0.0 0.5 --observables "spin_squared" --momentum 0 1
