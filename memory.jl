using LinearAlgebra
using SparseArrays

using Arpack
using JLD2
using FileIO
using ArgParse
using Memento
import Formatting
using DataStructures
using JSON
using HDF5
using Glob
using Parameters

using ExactDiagonalization
using TightBindingLattice

include("src/SquareJ1J2Jc.jl")


using .SquareJ1J2Jc: make_lattice,
                     make_bonds,
                     make_hilbert_space,
                     make_hamiltonian_terms,
                     make_spin_squared,
                     make_sz_sz_correlators

size_matrix = [ 4  0;
                0  4]
# size_matrix = [ 5  1;
#                -1  5]
allowed_quantum_numbers = 0:100
theta = [0.0, 0.0]

lattice = make_lattice(;size_matrix=size_matrix)
bonds = make_bonds(;hypercube=lattice.hypercube)
hilbert_space = make_hilbert_space(;hypercube=lattice.hypercube)
complete_quantum_numbers = quantum_number_sectors(hilbert_space)
sectors = sort(intersect(complete_quantum_numbers, allowed_quantum_numbers))

twoSz = sectors[1]

hilbert_space_representation = represent(HilbertSpaceSector(hilbert_space, twoSz))

kf = lattice.translation_group.fractional_momenta[1]
reduced_hilbert_space_representation = symmetry_reduce_parallel(hilbert_space_representation,
                                                                lattice.translation_group,
                                                                kf)
hamiltonian_terms = make_hamiltonian_terms(;
                        hypercube=lattice.hypercube,
                        nearest_neighbor_pairs=bonds.nearest_neighbor_pairs,
                        second_nearest_neighbor_pairs=bonds.second_nearest_neighbor_pairs,
                        chiral_triplets=bonds.chiral_triplets,
                        hilbert_space=hilbert_space,
                        theta=theta,
                    )
j1 = hamiltonian_terms.j1
j1_rep = represent(hilbert_space_representation, j1)
j1_redrep = represent(reduced_hilbert_space_representation, hamiltonian_terms.j1)
j1_sparse = sparse(j1_redrep)
j2_sparse = sparse(represent(reduced_hilbert_space_representation, hamiltonian_terms.j2))
jc_sparse = sparse(represent(reduced_hilbert_space_representation, hamiltonian_terms.jc))
