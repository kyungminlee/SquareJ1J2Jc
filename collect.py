import os
import argparse
import numpy as np
import pandas as pd
import h5py

def read_file(path):
    print("Reading file {}".format(path))
    items = []
    with h5py.File(path, 'r') as f:
        try:
            for groupname, group in f.items():
                eigenvalues = group['eigenvalues'][()]
                spin_squared = group['observables']['spin_squared'][()]
                J1 = group.attrs['J1']
                J2 = group.attrs['J2']
                Jc = group.attrs['Jc']
                twoSz = group.attrs['twoSz']
                momentum_index = group.attrs['momentum_index']
                theta = group.attrs['theta']
                for ev, sq in zip(eigenvalues, spin_squared):
                    items.append( (J1, J2, Jc, theta, twoSz, momentum_index, ev, sq))
        except KeyError as e:
            print(e)
            
    return items

def read_directory(path):
    print("Reading directory {}".format(path))
    items = []
    for fname in os.listdir(path):
        root, ext = os.path.splitext(fname)
        if ext != '.hdf5' and ext != '.h5': continue
        #print(os.path.join(path, fname))
        items += read_file(os.path.join(path, fname))
    return items


def collect_directory(root_directory):
    items = []
    for child in os.listdir(root_directory):
        root, ext = os.path.splitext(child)
        if ext != '.qed': continue
        
        childpath = os.path.join(root_directory, child)
        if not os.path.isdir(childpath): continue
        
        items += read_directory(childpath)
    df = pd.DataFrame(items, columns=['J1', 'J2','Jc',
                                      'theta', 'twoSz', 'momentum', 'eigenvalues', 'spin_squared'])
    return df

def main():
    parser = argparse.ArgumentParser('collect')
    parser.add_argument('root_directory', type=str)
    args = parser.parse_args()

    df = collect_directory(args.root_directory)
    df.to_csv("foo.csv")

if __name__=='__main__':
    main()
