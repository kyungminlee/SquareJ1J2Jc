module SquareJ1J2Jc

# export make_square_lattice
# export pauli_matrix
# export make_J1J2Jc_hamiltonian
# export make_Sz_Sz_correlators

using LinearAlgebra
using ExactDiagonalization
using TightBindingLattice


function pauli_matrix(hs::HilbertSpace, isite ::Integer, j ::Symbol)
  if j == :x
    return pure_operator(hs, isite, 1, 2, 1.0, UInt) + pure_operator(hs, isite, 2, 1, 1.0, UInt)
  elseif j == :y
    return pure_operator(hs, isite, 1, 2, -1.0im, UInt) + pure_operator(hs, isite, 2, 1, 1.0im, UInt)
  elseif j == :z
    return pure_operator(hs, isite, 1, 1, 1.0, UInt) + pure_operator(hs, isite, 2, 2, -1.0, UInt)
  elseif j == :+
    return pure_operator(hs, isite, 1, 2, 1.0, UInt)
  elseif j == :-
    return pure_operator(hs, isite, 2, 1, 1.0, UInt)
  else
    throw(ArgumentError("pauli matrix of type $(j) not supported"))
  end
end


function make_lattice(;size_matrix ::AbstractMatrix{<:Integer})
  hypercube = HypercubicLattice(size_matrix)

  generator_indices = get_generators(hypercube)
  generator_coordinates = hypercube.coordinates[generator_indices]
  generator_elements = [translation_element(hypercube, c) for c in generator_coordinates]
  translation_group = TranslationGroup(generator_elements)

  return (
    hypercube=hypercube,
    generator_indices=generator_indices,
    generator_coordinates=generator_coordinates,
    generator_elements=generator_elements,
    translation_group=translation_group,
  )
end


function make_bonds(;hypercube ::HypercubicLattice, periodic=true)
  n_sites = length(hypercube.coordinates)
  i2s(i::Integer) = hypercube.coordinates[i]
  s2i(sub::AbstractVector{<:Integer}) = hypercube.torus_wrap(sub)[2]

  nearest_neighbor_bond_types = [(1,0), (0,1)]
  nearest_neighbor_pairs = Tuple{Int, Int, Tuple{Int, Int}}[]
  for i in 1:n_sites, (d1, d2) in nearest_neighbor_bond_types
    i1, i2 = i2s(i)
    R, j = hypercube.torus_wrap([i1+d1, i2+d2])
    if periodic || all(R .== 0)
    #j = s2i([i1+d1, i2+d2])
      push!(nearest_neighbor_pairs, (i,j, (d1,d2)))
    end
  end

  second_nearest_neighbor_bond_types = [(1,1), (-1,1)]
  second_nearest_neighbor_pairs = Tuple{Int, Int, Tuple{Int, Int}}[]
  for i in 1:n_sites, (d1, d2) in second_nearest_neighbor_bond_types
    i1, i2 = i2s(i)
    R, j = hypercube.torus_wrap([i1+d1, i2+d2])
    if periodic || all(R .== 0)
      push!(second_nearest_neighbor_pairs, (i,j, (d1,d2)))
    end
  end

  chiral_triplet_types = [(( 1, 0), ( 0, 1)), (( 0, 1), (-1, 0)), ((-1, 0), ( 0,-1)), (( 0,-1), ( 1, 0)),]
  chiral_triplets = Tuple{Int64, Int64, Int64, Tuple{Int, Int}, Tuple{Int, Int}}[]
  for i in 1:n_sites, ((d1, d2), (e1, e2)) in chiral_triplet_types
    i1, i2 = i2s(i)
    j = s2i([i1+d1, i2+d2])
    k = s2i([i1+e1, i2+e2])
    Rj, j = hypercube.torus_wrap([i1+d1, i2+d2])
    Rk, k = hypercube.torus_wrap([i1+e1, i2+e2])
    if periodic || (all(Rj .== 0) && all(Rk .== 0))
      push!(chiral_triplets, (i,j,k, (d1,d2), (e1,e2)))
    end
  end

  return (nearest_neighbor_pairs=nearest_neighbor_pairs,
          second_nearest_neighbor_pairs=second_nearest_neighbor_pairs,
          chiral_triplets=chiral_triplets)
end


function make_hilbert_space(;hypercube::HypercubicLattice)
  n_sites = length(hypercube.coordinates)
  spin_site = Site([State("Up", 1), State("Dn", -1)])
  hilbert_space = HilbertSpace([spin_site for i in 1:n_sites])
  return hilbert_space
end



"""
J₁-J₂-Jᵪ Hamiltonian

```math
H = J_1 \\sum_{\\langle i, j \\rangle} S_{i} \\cdot S_{j} + ...
```
"""
function make_hamiltonian_terms(;hypercube ::HypercubicLattice,
                                nearest_neighbor_pairs,
                                second_nearest_neighbor_pairs,
                                chiral_triplets,
                                hilbert_space ::HilbertSpace{Int},
                                theta::AbstractVector{<:Real})

  @assert length(theta) == 2

  n_sites = length(hypercube.coordinates)

  hopping_phases = ones(ComplexF64, (3, 3))
  # TODO: check whether to transpose or not
  (theta_tilde_x, theta_tilde_y) = 2*pi*transpose(hypercube.inverse_scale_matrix) * theta

  for d1 in -1:1, d2 in -1:1
    hopping_phases[2+d1, 2+d2] = cis(d1 * theta_tilde_x + d2 * theta_tilde_y)
  end

  hopping_phase(d1 ::Int, d2 ::Int) ::ComplexF64 = return hopping_phases[2+d1, 2+d2]
  σ(i ::Integer, j ::Symbol) = pauli_matrix(hilbert_space, i, j)

  j1_terms = AbstractOperator[]
  for (i,j, (d1,d2)) in nearest_neighbor_pairs
    push!(j1_terms, σ(i, :+) * σ(j, :-) * (0.5 * hopping_phase(-d1, -d2)) )
    push!(j1_terms, σ(i, :-) * σ(j, :+) * (0.5 * hopping_phase(+d1, +d2)) )
    push!(j1_terms, σ(i, :z) * σ(j, :z) * 0.25)
  end

  j2_terms = AbstractOperator[]
  for (i,j, (d1,d2)) in second_nearest_neighbor_pairs
    push!(j2_terms, σ(i, :+) * σ(j, :-) * (0.5 * hopping_phase(-d1, -d2)) )
    push!(j2_terms, σ(i, :-) * σ(j, :+) * (0.5 * hopping_phase(+d1, +d2)) )
    push!(j2_terms, σ(i, :z) * σ(j, :z) * 0.25)
  end

  jc_terms  = AbstractOperator[]
  for (i, j, k, (d1, d2), (e1, e2)) in chiral_triplets
    push!(jc_terms, σ(i, :z) * σ(j, :+) * σ(k, :-) * ( 0.25*im * hopping_phase(d1-e1, d2-e2)) )
    push!(jc_terms, σ(i, :z) * σ(j, :-) * σ(k, :+) * (-0.25*im * hopping_phase(e1-d1, e2-d2)) )

    push!(jc_terms, σ(j, :z) * σ(k, :+) * σ(i, :-) * ( 0.25*im * hopping_phase( e1,  e2)) )
    push!(jc_terms, σ(j, :z) * σ(k, :-) * σ(i, :+) * (-0.25*im * hopping_phase(-e1, -e2)) )

    push!(jc_terms, σ(k, :z) * σ(i, :+) * σ(j, :-) * ( 0.25*im * hopping_phase(-d1, -d2)) )
    push!(jc_terms, σ(k, :z) * σ(i, :-) * σ(j, :+) * (-0.25*im * hopping_phase( d1,  d2)) )
  end

  j1 = isempty(j1_terms) ? NullOperator() : simplify(sum(j1_terms))
  j2 = isempty(j2_terms) ? NullOperator() : simplify(sum(j2_terms))
  jc = isempty(jc_terms) ? NullOperator() : simplify(sum(jc_terms))
  return (j1=j1, j2=j2, jc=jc)
end

function make_spin_squared(;hypercube ::HypercubicLattice,
                           hilbert_space::HilbertSpace{Int})
  n_sites = length(hypercube.coordinates)
  σ(i ::Integer, j ::Symbol) = pauli_matrix(hilbert_space, i, j)

  total_sx = sum(σ(i, :x) for i in 1:n_sites) * 0.5
  total_sy = sum(σ(i, :y) for i in 1:n_sites) * 0.5
  total_sz = sum(σ(i, :z) for i in 1:n_sites) * 0.5

  spin_squared = simplify(total_sx * total_sx + total_sy * total_sy + total_sz * total_sz)
  return spin_squared
end


function make_sz_sz_correlators(;hypercube ::HypercubicLattice, hilbert_space ::HilbertSpace{Int})
  n_sites = length(hypercube.coordinates)
  i2s(i::Integer) = hypercube.coordinates[i] ::Vector{Int}
  s2i(sub::AbstractVector{<:Integer}) = hypercube.torus_wrap(sub)[2] ::Int
  σ(i ::Integer, j ::Symbol) = pauli_matrix(hilbert_space, i, j)

  sz_sz_correlators = Dict{Tuple{Int, Int}, AbstractOperator}()
  for (d1, d2) in hypercube.coordinates
    operator_terms = []
    for (i, (i1, i2)) in enumerate(hypercube.coordinates)
      j = s2i([i1+d1,i2+d2])
      push!(operator_terms, σ(i, :z) * σ(j, :z))
    end
    sz_sz_correlators[d1, d2] = simplify(sum(operator_terms) * (0.25 / (n_sites)))
  end

  return sz_sz_correlators
end



end # module
