import os
import argparse
import numpy as np
import pandas as pd
import h5py
import tqdm

def read_file(path):
    #print('Reading file', path)
    theta_dim = -1
    momentum_dim = -1
    items = []
    with h5py.File(path, 'r') as f:
        #items = [x for x in f.items()]
        #for i in tqdm.trange(len(items)):
        #    groupname, group = items[i]
        #for groupname, group in tqdm.tqdm(f.items(), desc='file loop', leave=False):
        for groupname, group in f.items():
            try:
                eigenvalues = group['eigenvalues'][()]
                J1 = group.attrs['J1']
                J2 = group.attrs['J2']
                Jc = group.attrs['Jc']
                twoSz = group.attrs['twoSz']
                momentum = list(group.attrs['momentum_index'])
                theta = list(group.attrs['theta'])
                
                if 'observables' in group and 'spin_squared' in group['observables']:
                    spin_squared = group['observables']['spin_squared'][()]
                else:
                    spin_squared = [np.nan for x in eigenvalues]
                
                if theta_dim <= 0:
                    theta_dim = len(theta)
                else:
                    assert(theta_dim == len(theta))
                if momentum_dim <= 0:
                    momentum_dim = len(momentum)
                else:
                    assert(momentum_dim == len(momentum))
                for ev, sq in zip(eigenvalues, spin_squared):
                    items.append( (J1, J2, Jc, *theta, twoSz, *momentum, ev, sq))
            except KeyError as e:
                print(e)
            
    columns = ['J1', 'J2', 'Jc'] + ['theta_{}'.format(i+1) for i in range(theta_dim)] + ['twoSz'] + ['momentum_{}'.format(i+1) for i in range(momentum_dim)] + ['eigenvalue', 'spin_squared']
    if items:
        df = pd.DataFrame(items, columns=columns)
        return df
    else:
        return None

def read_directory(path):
    #print('Reading directory', path)
    items = []
    #fnames = os.listdir(path)
    #for i in tqdm.trange(len(fnames)):
    #    fname = fnames[i]
    #for fname in tqdm.tqdm(os.listdir(path), desc='directory loop', leave=False):
    for fname in os.listdir(path):
        root, ext = os.path.splitext(fname)
        if ext != '.hdf5' and ext != '.h5': continue
        #print(os.path.join(path, fname))
        item = read_file(os.path.join(path, fname))
        if item is not None:
            items.append(item)
    if items:
        return pd.concat(items)
    else:
        return None


def collect_directory(root_directory):
    #print('Collecting directory', root_directory)
    items = []
    #children = os.listdir(root_directory)
    #for i in tqdm.trange(len(children)):
    #    child = children[i]
    for child in tqdm.tqdm(os.listdir(root_directory), desc='collection'):
        root, ext = os.path.splitext(child)
        if ext != '.qed': continue
        
        childpath = os.path.join(root_directory, child)
        if not os.path.isdir(childpath): continue
        item = read_directory(childpath)
        if item is not None:
            items.append(item)
    return pd.concat(items)

def main():
    parser = argparse.ArgumentParser('collect')
    parser.add_argument('root_directory', type=str)
    args = parser.parse_args()

    df = collect_directory(args.root_directory)
    #df.to_hdf(os.path.join(args.root_directory, "summary.hdf5"), key='df', mode='w')
    df.to_csv(os.path.join(args.root_directory, "summary.csv"), index=False)

if __name__=='__main__':
    main()
