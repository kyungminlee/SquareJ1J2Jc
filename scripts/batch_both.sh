#!/bin/bash

n1s=(4 4 4 5 5)
n2s=(4 5 6 5 6)
qms=(0 0 0 1 0)
echo seq 1 "${n1s[@]}"
for i in $(seq 0 4)
do
  n1=${n1s[$i]}
  n2=${n2s[$i]}
  qm=${qms[$i]}
  for sz in 0 2 4 6 8 10 12
  do
    sz=$(($sz+$qm))
    echo "n1 = $n1, n2 = $n2, sz = $sz"

    spectrum_filename="spectrum_${n1}_${n2}_${sz}.jld2"
    structure_filename="structure_${n1}_${n2}_${sz}.jld2"

    if [ -f "$spectrum_filename" ]; then
      echo "$spectrum_filename exists. Skipping"
      continue
    fi

    #if [ ! -f "$structure_filename" ]; then
    #  echo "$structure_filename does not exist. Skipping"
    #  continue
    #fi

    echo "Running $structure_filename"

cat <<EOS > script
#!/bin/bash
#SBATCH --job-name=JC${n1}${n2}_${sz}
#SBATCH -n 24
#SBATCH -N 1 
#SBATCH -p genacc_q
#SBATCH -t 2-00:00:00
#SBATCH --mem-per-cpu=3900M

# -- GUARD -- #SBATCH --mail-user=klee@magnet.fsu.edu
# -- GUARD -- #SBATCH --mail-type=ALL

echo -n "## Hostname: "
hostname
echo -n "## Start Date: "
date

export JULIA_NUM_THREADS=24
export OMP_NUM_THREADS=24
export MKL_NUM_THREADS=24

julia main.jl --n1 $n1 --n2 $n2 --sz $sz

msgclient "Jd${n1}${n2}_${sz}" "Job \$SLURM_JOB_ID finished with $?"

EOS
sbatch script

done
done
