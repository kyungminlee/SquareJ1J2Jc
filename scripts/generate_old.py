import json
import numpy as np
import os
import itertools
import argparse


def main():

  parser = argparse.ArgumentParser()
  parser.add_argument("--m1", type=int, required=True)
  parser.add_argument("--m2", type=int, required=True)
  parser.add_argument("--J1", type=float, required=True, nargs="+")
  parser.add_argument("--J2", type=float, required=True, nargs="+")
  parser.add_argument("--Jc", type=float, required=True, nargs="+")
  args = parser.parse_args()

  m1 = args.m1
  m2 = args.m2

  # theta1s = np.linspace(0, 1, m1+1, endpoint=True)
  # theta2s = np.linspace(0, 1, m2+1, endpoint=True)
  # theta1s = np.linspace(0, 1, m1, endpoint=False)
  # theta2s = np.linspace(0, 1, m2, endpoint=False)
  #thetas = [[theta1, theta2] for theta1 in theta1s for theta2 in theta2s]
  thetas = [[0.0, 0.0]]

  J1s = args.J1
  J2s = args.J2
  Jcs = args.Jc
  for J1, J2, Jc in itertools.product(J1s, J2s, Jcs):
    for theta in thetas:
      parameters = {
        "shape": [4,4],
        "twoSz": 0,
        "J1": J1,
        "J2": J2,
        "Jc": Jc,
        "theta": theta,
        "num_eigens": 12,
        "save_eigenvectors": True,
        "observables": ["spin_squared", "sz_sz_correlators"]
      }

      dirname = "J1={J1:.3f}_J2={J2:.3f}_Jc={Jc:.3f}_theta1={theta[0]:.3f}_theta2={theta[1]:.3f}.qed".format(**parameters)
      filename = "parameters.json"

      os.makedirs(dirname, exist_ok=True)
      with open(os.path.join(dirname, filename), "w") as fp:
        json.dump(parameters, fp)

if __name__=='__main__':
  main()
