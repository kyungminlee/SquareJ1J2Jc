#!/bin/bash
#SBATCH --job-name=JC-test
#SBATCH -n 24
#SBATCH -N 1 
#SBATCH -p genacc_q
#SBATCH -t 2-00:00:00
#SBATCH --mem-per-cpu=3900M

# -- GUARD -- #SBATCH --mail-user=klee@magnet.fsu.edu
# -- GUARD -- #SBATCH --mail-type=ALL

echo -n "## Hostname: "
hostname
echo -n "## Start Date: "
date

export JULIA_NUM_THREADS=24
export OMP_NUM_THREADS=24
export MKL_NUM_THREADS=24

julia main.jl --n1 5 --n2 5 --sz 24

#msgclient "Jd56_12" "Job $SLURM_JOB_ID finished with 0"

