#
# # Example
#
# PS > Run.ps1  (Get-Item *.qed)

$projectDir = (Get-Item $PSCommandPath).Directory.Parent.FullName #(Split-Path -parent $PSCommandPath)
$julia = (Get-Command julia).Source
$script = "$projectDir\spectrum_momentum_2.jl"

if (-Not (Test-Path $script)) {
  Write-Host "$script does not exist"
  exit
} 

$jobs = $(Get-ChildItem *.qed -Directory | Where-Object {
  $firstFile = $_.FullName + "\spectrum_0_0x0.hdf5";
  (Test-Path $firstFile) -eq $false
})

#$jobs = $args[0]

foreach ($job in $jobs) {
  if (-Not (Test-Path $job)) {
    Write-Host "$job does not exist. Quitting."
    exit 1
  }
}

Write-Host ("{0} Jobs" -f $jobs.length)

$numGroups = 4
$counter = [pscustomobject] @{ Value = 0 }
$groups = $jobs | Group-Object -Property { $counter.Value++ % $numGroups }

foreach ($group in $groups) {
  [String[]]$files = $group.Group.FullName
  [String[]]$myArgs = @($script) + $files
  Start-Process -FilePath $julia -ArgumentList $myArgs -WorkingDirectory $PWD
  #julia $myArgs
}

# foreach ($group in $groups) {
#   $running = @(Get-Job | Where-Object { $_.State -eq 'Running' })
#   $args = @($julia, $script.Path, ($group.Group | ForEach-Object { $_.FullName }))
#   if ($running.Count -le 4) {
#     Start-Job -ScriptBlock {
#       Start-Process $args[0] $args[1..$args.length]
#       #Start-Process $julia $script $group.Groups
#     } -ArgumentList $args
#   } else {
#     $running | Wait-Job
#   }
#   Get-Job | Receive-Job
# }
# $running = @(Get-Job | Where-Object { $_.State -eq 'Running' })
# $running | Wait-Job
# Get-Job | Receive-Job


# foreach ($group in $groups) {
#   $julia $script  $group.Group
#   #echo "Bye"
# }
# for ($i = 0 ; $i -lt $jobs.length ; $i += $chunkSize) {
#   for ($j = 0 ; $j -lt $chunkSize ; $j += 1) {
#     echo $jobs[$i + $j]
#     echo "Bye"
#   }
# }
#echo $jobs
